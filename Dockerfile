FROM tiangolo/uwsgi-nginx-flask:python3.8
# RUN pip install --no-cache-dir matplotlib pandas

ENV UWSGI_CHEAPER 4
ENV UWSGI_PROCESSES 64
ENV NGINX_MAX_UPLOAD 128m
ENV LISTEN_PORT 8006
ENV UWSGI_INI uwsgi.ini
ENV NGINX_WORKER_CONNECTIONS 2048

## make a local directory
RUN mkdir -p /app

WORKDIR /app
COPY . /app

RUN pip install -r reqs.txt

CMD export FLASK_ENV=development
CMD export FLASK_APP=run.py
CMD export MYSQL_USER=root
CMD export MYSQL_PASSWORD=P4sswordnyalupa!
CMD export MYSQL_HOST=172.16.32.150
CMD export MYSQL_DATABASE=noc_rml_ikb
CMD export MYSQL_PORT=3306
CMD export SECRET_KEY='\x9c\xf5\xba\xd2Q\x9aP\x17\x8b\x8e9\xae5$\x08\xa0'

EXPOSE 8006

ENTRYPOINT ["/bin/bash", "-c", "flask run -h 0.0.0.0 -p 8006"]
CMD ["run.py"]
