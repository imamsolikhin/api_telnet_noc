"""
Database configuration file for the application.
"""
from config import Config
from sqlalchemy import create_engine, engine_from_config
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine(Config.DB_URI, convert_unicode=True,pool_size=1000,max_overflow=0)
db_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()

def init_db():
	from ..api.models import ClientVlanService, GemProfileLast, HostTrafficTableLast, Clients, Host, Ont, ServicePort, ClientsTools, HostLineProfile, OntInterface, ServicePortLast, Customer, HostLineProfileLast, OntInterfaceLast, Signal, DbaProfile, HostSrvProfile, OntLast, DbaProfileLast, HostSrvProfileLast, PortVlan, GemProfile, HostTrafficTable, PortVlanLast
	Base.metadata.create_all(bind=engine)
