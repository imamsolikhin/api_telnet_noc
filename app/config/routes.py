"""
Routing urls for the Api app
"""

# third-party imports
from flask import Blueprint
from flask_restful import Api

# local imports
from ..api.controller.host_controller import CheckHost
from ..api.controller.ont_controller import OntAuotFind
from ..api.controller.signal_controller import CheckSignal
from ..api.controller.monitor_controller import LoadConfig
from ..api.controller.rnd_controller import RnD
from ..api.controller.registration_controller import Activation
from ..api.controller.clients_controller import CheckClientsTools

api_app = Blueprint('api', __name__)
api = Api(api_app)

api.add_resource(CheckClientsTools, '/clientstools', methods=['GET'], endpoint='clientstools')
api.add_resource(CheckHost, '/checkhost', methods=['GET'], endpoint='checkhost')
api.add_resource(OntAuotFind, '/autofind', methods=['GET'], endpoint='autofind')
api.add_resource(CheckSignal, '/checksignal', methods=['GET'], endpoint='checksignal')
api.add_resource(Activation, '/activation', methods=['GET'], endpoint='activation')
api.add_resource(LoadConfig, '/loadconfig', methods=['GET'], endpoint='loadconfig')
api.add_resource(RnD, '/rnd', methods=['GET'], endpoint='rnd')
