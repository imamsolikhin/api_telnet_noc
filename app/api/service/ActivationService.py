"""
Schemas for the Api app
"""

# local imports
from ..models.GemProfile import GemProfile

class ActivationService():
	def getGemByVlan(self, host, profile, vlan, idx_i):
		resp = GemProfile(
					HostCode=host,
					ProfileId=profile,
					GemVlan=vlan
					).count_by_vlan()
		if resp != 0:
			resp = GemProfile(HostCode=host,ProfileId=profile,GemVlan=vlan).show_by_vlan()
			return resp[0].GemId

		else:
			idx = idx_i
			while True:
				resp = GemProfile(
						HostCode=host,
						ProfileId=profile,
						GemId=idx
						).count_by_id()

				if resp == 0:
					break
				idx = idx+1
			return idx
