"""
Views for the Api app
"""

# third-party imports
from ...helper.utils import response_json, make_request
from ...helper import constants
from ..models.Host import Host
from ..models.HostLineProfile import HostLineProfile
from ..models.HostLineProfileLast import HostLineProfileLast
from ..models.HostSrvProfile import HostSrvProfile
from ..models.HostSrvProfileLast import HostSrvProfileLast
from ..models.HostTrafficTable import HostTrafficTable
from ..models.HostTrafficTableLast import HostTrafficTableLast
from ..models.DbaProfile import DbaProfile
from ..models.DbaProfileLast import DbaProfileLast
from ..models.PortVlan import PortVlan
from ..models.PortVlanLast import PortVlanLast
from ..models.GemProfile import GemProfile
from ..models.GemProfileLast import GemProfileLast
from ..models.ServicePort import ServicePort
from ..models.ServicePortLast import ServicePortLast
from ..models.OntInterface import OntInterface
from ..models.OntInterfaceLast import OntInterfaceLast
from ..models.Ont import Ont
from ..models.OntLast import OntLast
from flask_restful import Resource, reqparse

from pprint import pprint
from collections import Counter

import array as arr
import re
import socket
import logging
import types
import six
import getpass
import sys
import telnetlib
import getopt
import ipaddress
import os
import time
import datetime
from flaskthreads import AppContextThread

KEYS = {
	'UP': '\033[B' ,
	'DW': '\033[C' ,
	'EN': '\r'
}

parser = reqparse.RequestParser()  # pylint: disable=invalid-name
parser.add_argument('code' , type=str)
parser.add_argument('config' , type=str)


class CheckHost(Resource):
	code = None
	config = None
	tn = None
	IpAddress = None
	Hostname = None
	usr = None
	psw = None
	port = None
	hasecho = False
	status = True
	message = None
	response = None
	data_cmd = None
	data = None

	m_host = None

	def get(self):
		args = parser.parse_args()
		self.code = args['code']
		self.config = args['config']
		result = Host(Code=self.code).show()
		self.m_host = result[0]
		for rs in result:
			self.code = rs.Code
			self.IpAddress = rs.IpAddress
			self.Hostname = rs.Hostname
			self.usr = rs.Username
			self.psw = rs.Password
			self.port = rs.Port

		self.connect()

		if self.tn:
			self.response = []
			self.data_cmd = []
			self.data_cmd.append('enable')
			self.data_cmd.append('scroll 512')
			self.runCommand()

			if self.config != None:
				if "current-config" in self.config:
					self.runExcecute('display current-configuration')
					# print('display current-configuration')
					thread = AppContextThread(target=self.runCommandCurrentConfig())
					thread.start()
					thread.join()

				if "traffic-table" in self.config or "all" in self.config:
					self.runExcecute('display current-configuration section global-config')
					# print('display current-configuration section global-config')
					thread = AppContextThread(target=self.runCommandTrafficTable())
					thread.start()
					thread.join()

				if "line-profile" in self.config or "all" in self.config:
					self.runExcecute('display ont-lineprofile gpon all')
					# print('display ont-lineprofile gpon all')
					thread = AppContextThread(target=self.runCommandLineProfile())
					thread.start()
					thread.join()

				if "srv-profile" in self.config or "all" in self.config:
					self.runExcecute('display ont-srvprofile gpon all')
					# print('display ont-srvprofile gpon all')
					thread = AppContextThread(target=self.runCommandSrvProfile())
					thread.start()
					thread.join()

				if "port-vlan" in self.config or "all" in self.config:
					self.runExcecute('display current-configuration section vlan-config')
					# print('display current-configuration section vlan-config')
					thread = AppContextThread(target=self.runCommandPortVlan())
					thread.start()
					thread.join()

				if "dba-profile" in self.config or "all" in self.config:
					self.runExcecute('display dba-profile all')
					# print('display dba-profile all')
					thread = AppContextThread(target=self.runCommandDbaProfile())
					thread.start()
					thread.join()

				if "service-port" in self.config or "all" in self.config:
					self.runExcecute('display service-port all')
					# print('display service-port all')
					thread = AppContextThread(target=self.runCommandServicePort())
					thread.start()
					thread.join()

				if "gem-port" in self.config or "all" in self.config:
					rst_lp = HostLineProfile(HostCode=self.code).show()
					result = GemProfile(HostCode=self.code).show()
					if GemProfileLast(HostCode=self.code).count():
						GemProfileLast(HostCode=self.code).delete()
					for rs in result:
						GemProfileLast(
							Code=rs.Code,
							HostCode=rs.HostCode,
							ProfileId=rs.ProfileId,
							GemId=rs.GemId,
							GemMapping=rs.GemMapping,
							GemVlan=rs.GemVlan,
							Priority=rs.Priority,
							Type=rs.Type,
							Port=rs.Port,
							Bundle=rs.Bundle,
							Flow=rs.Flow,
							Transparent=rs.Transparent,
							ActiveStatus=rs.ActiveStatus,
							CreatedBy=rs.CreatedBy,
							CreatedDate=rs.CreatedDate
						).save()

					if GemProfile(HostCode=self.code).count():
						GemProfile(HostCode=self.code).delete()
					for x in range(0, len(rst_lp)):
						if rst_lp[x].ProfileId:
							self.runExcecute('display ont-lineprofile gpon profile-id ' + str(rst_lp[x].ProfileId))
							# print('display ont-lineprofile gpon profile-id ' + str(rst_lp[x].ProfileId))
							thread = AppContextThread(target=self.runCommandGemPortProfile(rst_lp[x].ProfileId))
							thread.start()
							thread.join()
					self.response.append("Gem Index is updated from " + str(self.Hostname)+ " IP:" +str(self.IpAddress))

				if "ont-info" in self.config or "all" in self.config:
					self.runExcecute('display ont info 0 all')
					# print('display ont info 0 all')
					thread = AppContextThread(target=self.runCommandOntInterface())
					thread.start()
					thread.join()

				if "ont-autofind" in self.config:
					self.runExcecute('display ont autofind all')
					# print('display ont autofind all')
					thread = AppContextThread(target=self.runCommandOntFindAll())
					thread.start()
					thread.join()

		self.disconnect()
		return response_json(self.status, self.message, self.response)

	def connect(self):
		try:
			self.tn = telnetlib.Telnet(self.IpAddress, self.port)
		except socket.error:
			self.status = False
			self.message = "Problem connecting to host ('telnet %s %s')" % (
				self.IpAddress, self.port)

		if self.status:
			time.sleep(.2)
			# self.tn.set_debuglevel(200)
			self.tn.read_until(b"name:")
			self.tn.write(self.usr.encode("ascii")+b"\n")
			self.tn.read_until(b"password:")
			self.tn.write(self.psw.encode("ascii")+b"\n")
			time.sleep(.2)
			self.tn.write(b"y\n")
			self.tn.write(b"\n")
			time.sleep(.2)

			self.response = self.tn.read_very_eager().decode("ascii")

			if "invalid" in self.response:
				self.status = False
				self.message = "Username or password is not valid"
			else:
				self.status = True
				self.message = "Username or password is valid"

	def disconnect(self):
		if self.tn:
			self.tn.close()
			self.tn = None

	def runCommand(self):
		self.response = []
		OntDistance = ""
		OntRange = ""
		RxPower = ""
		TxPower = ""

		for i in range(0, len(self.data_cmd)):
			self.tn.write(telnetlib.IAC + telnetlib.WILL + telnetlib.ECHO)
			self.tn.write(self.data_cmd[i].encode("ascii")+b'\n')
			time.sleep(len(self.data_cmd[i])/25)
			while True:
				data = self.tn.read_very_eager().decode("ascii")
				if data:
					data = data.replace("'", "").replace("\r", "").split("\n")
					if "#" in data[len(data)-1]:
						break
					elif "More" in data[len(data)-1] or "" in data[len(data)-1]:
						self.tn.write(" ".encode("ascii")+b"\n")
						time.sleep(0.2)
					else:
						self.tn.write(" ".encode("ascii")+b"\n")
						time.sleep(0.2)
				else:
					break

	def runExcecute(self,cmd):
		if self.tn:
			self.tn.write(cmd.encode("ascii")+b"\n")
			time.sleep(.2)
			self.tn.write(" ".encode("ascii")+b"\n")
			time.sleep(.2)
			self.tn.write(" ".encode("ascii")+b"\n")
			time.sleep(len(cmd)/20)
			dt = ""
			resp = self.tn.read_very_eager().decode("ascii")
			print("here runExcecute: "+cmd)
			while True:
				dt = str(dt) +"\r\n"+ str(resp)
				datas = resp.replace("\r", "").split("\n")
				if " Press 'Q' to break " in resp or " Press 'Q' to break " in datas[len(datas)-1] or datas[len(datas)-1].replace(" ", "") == "":
					self.tn.write(" ".encode("ascii")+b"\n")
					time.sleep(1)
					resp = self.tn.read_very_eager().decode("ascii")
				elif "#" in datas[len(datas)-1] or "return" in datas[len(datas)-1]:
					break
				else:
					break
			self.data = dt.replace("\u001b[37D                                     \u001b[37D", "").replace("---- More ( Press 'Q' to break ) ----", "").replace("\r", "").split("\n")

	def runCommandCurrentConfig(self):
		x=0
		dt_l = len(self.data)
		resp = []
		while x < dt_l:
			if self.data[x].replace(" ", "") != "":
				if self.data[x].replace(" ", "") != "#":
					resp.append(self.data[x].replace("<", "[").replace(">", "]"))
			x=x+1
		result = "\n".join(self.data)
		source = Host(Code=self.code, CheckStatus=self.message, CurrentConfig=result).update_config()
		self.response.append("Current Config Table is updated from " + str(self.Hostname)+ " IP:" +str(self.IpAddress))
		# self.response.append(result)

	def runCommandTrafficTable(self):
		result = HostTrafficTable(HostCode=self.code).show()
		if HostTrafficTableLast(HostCode=self.code).count():
			HostTrafficTableLast(HostCode=self.code).delete()
		for rs in result:
			HostTrafficTableLast(
				Code=rs.Code,
				HostCode=rs.HostCode,
				Index=rs.Index,
				Name=rs.Name,
				Cir=rs.Cir,
				Cbs=rs.Cbs,
				Pir=rs.Pir,
				Pbs=rs.Pbs,
				Priority=rs.Priority,
				ActiveStatus=rs.ActiveStatus,
				CreatedBy=rs.CreatedBy,
				CreatedDate=rs.CreatedDate
			).save()

		if HostTrafficTable(HostCode=self.code).count():
			HostTrafficTable(HostCode=self.code).delete()
		resp = []
		for x in range(0, len(self.data)):
			if self.data[x] != "":
				if "traffic table" in self.data[x]:
					resp.append(self.data[x])
					data_resp = re.sub(' +' , ' ' , self.data[x]).lstrip().split(" ")
					list_traffic = ["", "", "", "", "", "", ""]
					for i in range(0, len(data_resp)):
						if data_resp[i] != "":
							if data_resp[i] == "index":
								list_traffic[0] = data_resp[i+1]
							if data_resp[i] == "name":
								list_traffic[1] = data_resp[i+1]
							if data_resp[i] == "cir":
								list_traffic[2] = data_resp[i+1]
							if data_resp[i] == "cbs":
								list_traffic[3] = data_resp[i+1]
							if data_resp[i] == "pir":
								list_traffic[4] = data_resp[i+1]
							if data_resp[i] == "pbs":
								list_traffic[5] = data_resp[i+1]
							if data_resp[i] == "priority":
								list_traffic[6] = data_resp[i+1]

					# HostTrafficTable(Code=str(self.code)+ "-" +str(list_traffic[0])).delete_by_code()
					source = HostTrafficTable(
						Code=str(self.code)+ "-" +str(list_traffic[0]+"-"+str(x)),
						HostCode=self.code,
						Index=list_traffic[0],
						Name=list_traffic[1],
						Cir=list_traffic[2],
						Cbs=list_traffic[3],
						Pir=list_traffic[4],
						Pbs=list_traffic[5],
						Priority=list_traffic[6],
						ActiveStatus=1
					).save()
		self.response.append("Traffict Table is updated from " + str(self.Hostname)+ " IP:" +str(self.IpAddress))

	def runCommandLineProfile(self):
		result = HostLineProfile(HostCode=self.code).show()
		if HostLineProfileLast(HostCode=self.code).count():
			HostLineProfileLast(HostCode=self.code).delete()
		for rs in result:
			HostLineProfileLast(
				Code=rs.Code,
				HostCode=rs.HostCode,
				ProfileId=rs.ProfileId,
				ProfileName=rs.ProfileName,
				ProfileBinding=rs.ProfileBinding,
				ActiveStatus=rs.ActiveStatus,
				CreatedBy=rs.CreatedBy,
				CreatedDate=rs.CreatedDate
			).save()
		if HostLineProfile(HostCode=self.code).count():
			HostLineProfile(HostCode=self.code).delete()

		resp = []
		for x in range(0, len(self.data)):
			if self.data[x] != "":
				# print(self.data[x])
				resp.append(self.data[x])
				if "Profile-ID" in self.data[x]:
					x = x+1
				data_resp = re.sub(' +' , ' ' , self.data[x]).lstrip().split(" ")
				if data_resp:
					if len(data_resp) == 4:
						if data_resp[0] != "display" and data_resp[0] != "{":
							source = HostLineProfile(
								Code=str(self.code)+ "-" +str(data_resp[0]),
								HostCode=self.code,
								ProfileId=data_resp[0],
								ProfileName=data_resp[1],
								ProfileBinding=data_resp[2],
								ActiveStatus=1
							).save()
		self.response.append("Line Profile is updated from " + str(self.Hostname)+ " IP:" +str(self.IpAddress))

	def runCommandSrvProfile(self):
		result = HostSrvProfile(HostCode=self.code).show()
		if HostSrvProfileLast(HostCode=self.code).count():
			HostSrvProfileLast(HostCode=self.code).delete()
		for rs in result:
			HostSrvProfileLast(
				Code=rs.Code,
				HostCode=rs.HostCode,
				ProfileId=rs.ProfileId,
				ProfileName=rs.ProfileName,
				ProfileBinding=rs.ProfileBinding,
				ActiveStatus=rs.ActiveStatus,
				CreatedBy=rs.CreatedBy,
				CreatedDate=rs.CreatedDate
			).save()
		if HostSrvProfile(HostCode=self.code).count():
			HostSrvProfile(HostCode=self.code).delete()

		resp = []
		for x in range(0, len(self.data)):
			if self.data[x] != "":
				resp.append(self.data[x])
				if "Profile-ID" in self.data[x]:
					x = x+1

				data_resp = re.sub(' +' , ' ' , self.data[x]).lstrip().split(" ")
				if data_resp:
					if len(data_resp) == 4:
						if data_resp[0] != "display" and data_resp[0] != "{":
							source = HostSrvProfile(
								Code=str(self.code)+ "-" +str(data_resp[0]),
								HostCode=self.code,
								ProfileId=data_resp[0],
								ProfileName=data_resp[1],
								ProfileBinding=data_resp[2],
								ActiveStatus=1
							).save()
		self.response.append("Service Profile is updated from " + str(self.Hostname)+ " IP:" +str(self.IpAddress))

	def runCommandPortVlan(self):
		result = PortVlan(HostCode=self.code).show()
		if PortVlanLast(HostCode=self.code).count():
			PortVlanLast(HostCode=self.code).delete()
		for rs in result:
			PortVlanLast(
				Code=rs.Code,
				HostCode=rs.HostCode,
				Index=rs.Index,
				Type=rs.Type,
				Desc=rs.Desc,
				Attrib=rs.Attrib,
				Frame=rs.Frame,
				Slot=rs.Slot,
				Port=rs.Port,
				ActiveStatus=rs.ActiveStatus,
				CreatedBy=rs.CreatedBy,
				CreatedDate=rs.CreatedDate
			).save()
		if PortVlan(HostCode=self.code).count():
			PortVlan(HostCode=self.code).delete()

		vlan_id = []
		vlan_type = []
		vlan_desc = []
		vlan_attr = []
		vlan_port = []
		resp = []
		for x in range(0, len(self.data)):
			if self.data[x] != "":
				resp.append(self.data[x])
				if "vlan-config" in self.data[x]:
					x = x+1
				if x < len(self.data):
					data_vald = re.sub(' +' , ' ' , self.data[x]).lstrip().split(" ")
					if data_vald[0] == "vlan" and data_vald[1] != "desc" and data_vald[1] != "attrib":
						if " to " in self.data[x]:
							i = data_vald[1]
							for i in range(int(data_vald[1]), int(data_vald[3])+1):
								vlan_id.append(i)
								vlan_type.append(data_vald[4])
								vlan_desc.append("")
								vlan_attr.append("")
								vlan_port.append("///")
						else:
							vlan_id.append(data_vald[1])
							vlan_type.append(data_vald[2])
							vlan_desc.append("")
							vlan_attr.append("")
							vlan_port.append("///")

					if data_vald[0] == "vlan" and data_vald[1] == "desc":
						for y in range(0, len(vlan_id)):
							if " to " in self.data[x]:
								i = int(data_vald[2])
								for i in range(int(data_vald[2]), int(data_vald[4])+1):
									if int(vlan_id[y]) == i:
										vlan_desc[y] = data_vald[6]
							elif int(vlan_id[y]) == int(data_vald[2]):
								vlan_desc[y] = data_vald[4]

					if data_vald[0] == "vlan" and data_vald[1] == "attrib":
						for y in range(0, len(vlan_id)):
							if " to " in self.data[x]:
								i = int(data_vald[2])
								for i in range(int(data_vald[2]), int(data_vald[4])+1):
									if int(vlan_id[y]) == i:
										vlan_attr[y] = data_vald[5]
							elif int(vlan_id[y]) == int(data_vald[2]):
								vlan_attr[y] = data_vald[3]
					if data_vald[0] == "port" and data_vald[1] == "vlan":
						for y in range(0, len(vlan_id)):
							if " to " in self.data[x]:
								i = int(data_vald[2])
								for i in range(int(data_vald[2]), int(data_vald[4])+1):
									if int(vlan_id[y]) == i:
										vlan_port[y] = str(data_vald[5])+ "/" +str(data_vald[6])
							elif int(vlan_id[y]) == int(data_vald[2]):
								vlan_port[y] = str(data_vald[3])+ "/" +str(data_vald[4])
				x = x+1

		db_input = []
		for z in range(0, len(vlan_id)):
			if vlan_id[z] not in db_input:
				source = PortVlan(
					Code=str(self.code)+ "-" +str(vlan_id[z]),
					HostCode=self.code,
					Index=vlan_id[z],
					Type=vlan_type[z],
					Desc=vlan_desc[z],
					Attrib=vlan_attr[z],
					Frame=vlan_port[z].split("/")[0],
					Slot=vlan_port[z].split("/")[1],
					Port=vlan_port[z].split("/")[2],
					ActiveStatus=1
				).save()
			db_input.append(vlan_id[z])
		self.response.append("Vlan is updated from " + str(self.Hostname)+ " IP:" +str(self.IpAddress))

	def runCommandDbaProfile(self):
		result = DbaProfile(HostCode=self.code).show()
		if DbaProfileLast(HostCode=self.code).count():
			DbaProfileLast(HostCode=self.code).delete()
		for rs in result:
			DbaProfileLast(
				Code=rs.Code,
				HostCode=rs.HostCode,
				ProfileId=rs.ProfileId,
				Type=rs.Type,
				Bandwidth=rs.Bandwidth,
				Fix=rs.Fix,
				Assure=rs.Assure,
				Max=rs.Max,
				Bind=rs.Bind,
				ActiveStatus=rs.ActiveStatus,
				CreatedBy=rs.CreatedBy,
				CreatedDate=rs.CreatedDate
			).save()
		if DbaProfile(HostCode=self.code).count():
			DbaProfile(HostCode=self.code).delete()

		resp = []
		for x in range(0, len(self.data)):
			if self.data[x] != "":
				resp.append(self.data[x])
				if "Profile-ID" in self.data[x]:
					x = x+1

				data_resp = re.sub(' +' , ' ' , self.data[x]).lstrip().split(" ")
				if data_resp:
					if len(data_resp) == 7:
						source = DbaProfile(
							Code=str(self.code)+ "-" +str(data_resp[0]),
							HostCode=self.code,
							ProfileId=data_resp[0],
							Type=data_resp[1],
							Bandwidth=data_resp[2],
							Fix=data_resp[3],
							Assure=data_resp[4],
							Max=data_resp[5],
							Bind=data_resp[6],
							ActiveStatus=1
						).save()
		self.response.append("DBA Profile is updated from " + str(self.Hostname)+ " IP:" +str(self.IpAddress))

	def runCommandServicePort(self):
		result = ServicePort(HostCode=self.code).show()
		if ServicePortLast(HostCode=self.code).count():
			ServicePortLast(HostCode=self.code).delete()
		for rs in result:
			ServicePortLast(
				Code=rs.Code,
				HostCode=rs.HostCode,
				Index=rs.Index,
				VlanId=rs.VlanId,
				VlanAttr=rs.VlanAttr,
				Type=rs.Type,
				FrameId=rs.FrameId,
				SlotId=rs.SlotId,
				PortId=rs.PortId,
				VPI=rs.VPI,
				VCI=rs.VCI,
				FlowType=rs.FlowType,
				FlowPara=rs.FlowPara,
				Rx=rs.Rx,
				Tx=rs.Tx,
				State=rs.State,
				ActiveStatus=rs.ActiveStatus,
				CreatedBy=rs.CreatedBy,
				CreatedDate=rs.CreatedDate
			).save()
		if ServicePort(HostCode=self.code).count():
			ServicePort(HostCode=self.code).delete()

		x = 0
		resp = []
		while x < len(self.data):
			if self.data[x] != "":
				resp.append(self.data[x])
				data_resp = re.sub(' +' , ' ' , self.data[x]).lstrip().split(" ")

				if "Total :" in self.data[x]:
					break
				if data_resp:
					if len(data_resp) >= 13 and len(data_resp) <= 14:
						if (data_resp[0] != "INDEX"):
							if ServicePort(Code=str(self.code)+ "-" +str(data_resp[0])+str(data_resp[7])).count_id():
								ServicePort(Code=str(self.code)+ "-" +str(data_resp[0])+str(data_resp[7])).delete_id()
							# print(data_resp)
							source = ServicePort(
								Code=str(self.code)+ "-" +str(data_resp[0])+str(data_resp[7]),
								HostCode=self.code,
								Index=data_resp[0],
								VlanId=data_resp[1],
								VlanAttr=data_resp[2],
								Type=data_resp[3],
								FrameId=data_resp[4].split("/")[0],
								SlotId=data_resp[4].split("/")[1],
								PortId=data_resp[5].replace("/", ""),
								VPI=data_resp[6],
								VCI=data_resp[7],
								FlowType=data_resp[8],
								FlowPara=data_resp[9],
								Rx=data_resp[10],
								Tx=data_resp[11],
								State=data_resp[12],
								ActiveStatus=1
							)
							source.save()
			x = x+1
		self.response.append("Service Port is updated from " + str(self.Hostname)+ " IP:" +str(self.IpAddress))

	def runCommandGemPortProfile(self,id_lp):
		x = 0
		gemid = 0
		resp = []
		lp_id = []
		while x < len(self.data):
			if self.data[x] != "":
				resp.append(self.data[x])
				if "Gem Index" in self.data[x]:
					gemid = re.sub(' +' , ' ' , self.data[x]).lstrip().split(" ")[2].replace('>' , '')
				if "Mapping VLAN" in self.data[x]:
					while True:
						if x == len(self.data):
							break
						if "Gem Index" in self.data[x]:
							gemid = re.sub(' +' , ' ' , self.data[x]).lstrip().split(" ")[2].replace('>' , '')

						data_map = re.sub(' +' , ' ' , self.data[x]).lstrip().split(" ")
						if len(data_map) == 9:
							if str(self.code)+ "-" + id_lp+ "-" +str(gemid)+ "-" +str(data_map[1]+ "-" +str(x)) not in lp_id:
								if GemProfile(Code=str(self.code)+ "-" + id_lp+ "-" +str(gemid)+ "-" +str(data_map[1]+ "-" +str(x))).count_id():
									GemProfile(Code=str(self.code)+ "-" + id_lp+ "-" +str(gemid)+ "-" +str(data_map[1]+ "-" +str(x))).delete_by_code()
								GemProfile(
									Code=str(self.code)+ "-" + id_lp+ "-" +str(gemid)+ "-" +str(data_map[1]+ "-" +str(x)),
									HostCode=self.code,
									ProfileId=id_lp,
									GemId=gemid,
									GemMapping=data_map[0],
									GemVlan=data_map[1],
									Priority=data_map[2],
									Type=data_map[3],
									Port=data_map[4],
									Bundle=data_map[5],
									Flow=data_map[6],
									Transparent=data_map[7],
									ActiveStatus=1
								).save()
								lp_id.append(str(self.code)+ "-" + id_lp+ "-" +str(gemid)+ "-" +str(data_map[1]+ "-" +str(x)))
						if "Binding" in self.data[x]:
							break
						x = x+1
			x = x+1

	def runCommandOntInterface(self):
		result = OntInterface(HostCode=self.code).show()
		if OntInterfaceLast(HostCode=self.code).count():
			OntInterfaceLast(HostCode=self.code).delete()
		for rs in result:
			OntInterfaceLast(
				Code=rs.Code,
				HostCode=rs.HostCode,
				FrameId = rs.FrameId,
				SlotId = rs.SlotId,
				PortId = rs.PortId,
				OntId = rs.OntId,
				OntSn = rs.OntSn,
				Flag = rs.Flag,
				Run = rs.Run,
				Config = rs.Config,
				Match = rs.Match,
				Protect = rs.Protect,
				ActiveStatus=rs.ActiveStatus,
				CreatedBy=rs.CreatedBy,
				CreatedDate=rs.CreatedDate
			).save()
		if OntInterface(HostCode=self.code).count():
			OntInterface(HostCode=self.code).delete()

		resp = []
		for x in range(0, len(self.data)):
			if self.data[x] != "" and x < len(self.data):
				resp.append(re.sub(' +' , ' ' , self.data[x]).lstrip())
				data_map = re.sub(' +' , ' ' , self.data[x]).lstrip().split(" ")
				if data_map:
					# print(self.data[x])
					if len(data_map) >= 9 and len(data_map) <= 10:
						if len(data_map) == 9 and (data_map[7] == "no" and data_map[8] == ""):
							print([len(data_map),data_map])
							OntInterface(
								Code=str(self.code)+ "-" + data_map[2] + "-" +str(x),
								HostCode=self.code,
								FrameId = data_map[0].lstrip().split("/")[0],
								SlotId = data_map[0].lstrip().split("/")[1],
								PortId = data_map[0].lstrip().split("/")[2],
								OntId =data_map[1],
								OntSn = data_map[2],
								Flag =data_map[3],
								Run =data_map[4],
								Config =data_map[5],
								Match =data_map[6],
								Protect =data_map[7],
								ActiveStatus=1
							).save()
						elif len(data_map) == 10 and (data_map[8] == "no" and data_map[9] == ""):
							print([len(data_map),data_map])
							OntInterface(
								Code=str(self.code)+ "-" + data_map[2] + "-" +str(x),
								HostCode=self.code,
								FrameId = data_map[0].lstrip().split("/")[0],
								SlotId = data_map[1].lstrip().split("/")[0],
								PortId = data_map[1].lstrip().split("/")[1],
								OntId =data_map[2],
								OntSn = data_map[3],
								Flag =data_map[4],
								Run =data_map[5],
								Config =data_map[6],
								Match =data_map[7],
								Protect =data_map[8],
								ActiveStatus=1
							).save()
		self.response.append("Interface Gpon ONT is updated from " + str(self.Hostname)+ " IP:" +str(self.IpAddress))

	def runCommandOntFindAll(self):
		result = Ont(HostCode=self.code).show()
		if OntLast(HostCode=self.code).count():
			OntLast(HostCode=self.code).delete()
		for rs in result:
			OntLast(
				Code = rs.Code,
				HostCode = rs.HostCode,
				Sn = rs.Sn,
				Version = rs.Version,
				SoftwareVersion = rs.SoftwareVersion,
				EquipmentId = rs.EquipmentId,
				FrameId = rs.FrameId,
				SlotId = rs.SlotId,
				PortId = rs.PortId,
				ActiveStatus = rs.ActiveStatus,
				CreatedBy=rs.CreatedBy,
				CreatedDate=rs.CreatedDate
			).save()
		if Ont(HostCode=self.code).count_by_host():
			Ont(HostCode=self.code).delete()
		prmNo = 0
		prmHostCode = ""
		prmSn = ""
		prmVersion = ""
		prmSoftwareVersion = ""
		prmEquipmentId = ""
		prmFrameId = ""
		prmSlotId = ""
		prmPortId = ""
		resp = []
		for x in range(0, len(self.data)):
			if self.data[x] != "":
				resp.append(self.data[x])
				ont_list = ["Number","Ont SN","Ont Version","F/S/P","Ont EquipmentID","Ont SoftwareVersion"]
				prmHostCode = self.code
				if ont_list[0] in self.data[x]:
					prmNo = self.data[x].replace(ont_list[0],"").replace(":","").lstrip()
				elif ont_list[1] in self.data[x]:
					prmSn = (self.data[x].replace(ont_list[1],"").replace(":","").lstrip().split())[0]
				elif ont_list[2] in self.data[x]:
					prmVersion = self.data[x].replace(ont_list[2],"").replace(":","").lstrip()
				elif ont_list[3] in self.data[x]:
					data_fps = self.data[x].replace(ont_list[4],"").lstrip()
					data_arr = data_fps.split(":")[1].split("/")
					prmFrameId = data_arr[0].lstrip()
					prmSlotId = data_arr[1].lstrip()
					prmPortId = data_arr[2].lstrip()
				elif ont_list[4] in self.data[x]:
					prmEquipmentId = self.data[x].replace(ont_list[4],"").replace(":","").lstrip()
				elif ont_list[5] in self.data[x]:
					prmSoftwareVersion = self.data[x].replace(ont_list[5],"").replace(":","").lstrip()

				if prmHostCode !="" and prmSn !="" and prmVersion !="" and prmSoftwareVersion !="" and prmEquipmentId !="" and prmFrameId !="" and prmSlotId !="" and prmPortId !="":
					source = Ont(
						Code = prmSn+prmHostCode,
						HostCode = prmHostCode,
						Sn = prmSn,
						Version = prmVersion,
						SoftwareVersion = prmSoftwareVersion,
						EquipmentId = prmEquipmentId,
						FrameId = prmFrameId,
						SlotId = prmSlotId,
						PortId = prmPortId,
						ActiveStatus = 1
					)
					source.save()
					prmNo = 0
					prmHostCode = ""
					prmSn = ""
					prmVersion = ""
					prmSoftwareVersion = ""
					prmEquipmentId = ""
					prmFrameId = ""
					prmSlotId = ""
					prmPortId = ""

		self.response.append("ONT IDLE is updated from " + str(self.Hostname)+ " IP:" +str(self.IpAddress))
