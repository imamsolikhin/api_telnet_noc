"""
Views for the Api app
"""

# third-party imports
from ...helper.utils import response_json, make_request
from ...helper import constants
from ..models.Host import Host
from flask_restful import Resource, reqparse

from pprint import pprint
from collections import Counter

import array as arr
import re, socket, logging, types, six, getpass, sys, telnetlib, getopt, ipaddress, os, time, datetime
from flaskthreads import AppContextThread

KEYS = {
	'UP': '\033[B',
	'DW': '\033[C',
	'EN': '\r'
}

parser = reqparse.RequestParser()  # pylint: disable=invalid-name
parser.add_argument('code', type=str)

class LoadConfig(Resource):
	code        	= None
	tn        		= None
	hostname        = None
	usr				= None
	psw				= None
	port            = None
	hasecho         = False
	status			= True
	message			= None
	response		= None
	data_cmd		= None

	def get(self):
		args = parser.parse_args()
		self.code = args['code']
		thread = AppContextThread(target=self.function())
		thread.start()
		thread.join()

	def function(self):
		result = Host(Code=self.code).show()
		for rs in result:
			self.code = rs.Code
			self.hostname = rs.IpAddress
			self.usr = rs.Username
			self.psw = rs.Password
			self.port = rs.Port

		self.connect()
		if self.tn:
			# self.tn.set_debuglevel(1)
			# self.tn.write(telnetlib.IAC + telnetlib.WILL + telnetlib.ECHO)
			# self.tn.write(b"enable\n")
			# # time.sleep(.1)
			# self.tn.write(b"scroll 512\n")
			# # time.sleep(.1)
			# self.tn.write(b"display current-configuration simple section vlan-config\n")
			# # time.sleep(.3)
			# self.tn.read_until(b":")
			# self.tn.write(KEYS['EN'].encode("ascii")+b"\n")
			# self.tn.read_until(b"return")
			# self.response = self.tn.read_all().decode("ascii")
			# print(self.response)

			self.tn.set_debuglevel(1)
			self.data_cmd = []
			self.data_cmd.append('enable')
			self.data_cmd.append('scroll 512')
			self.data_cmd.append('display current-configuration simple section vlan-config')
			self.runCommand()


		self.disconnect()

		return response_json(self.status, self.message, self.response)

	def connect(self):
		try:
			self.tn = telnetlib.Telnet(self.hostname,self.port)
		except socket.error:
			self.status = False
			self.message = "Problem connecting to host ('telnet %s %s')" % (self.hostname, self.port)

		if self.status:
			self.tn.read_until(b"name:")
			self.tn.write(self.usr.encode("ascii")+b"\n")
			self.tn.read_until(b"password:")
			self.tn.write(self.psw.encode("ascii")+b"\n")
			time.sleep(.3)
			self.tn.write(b"y\n")
			self.tn.write(b"\n")
			time.sleep(.3)

			self.response = self.tn.read_very_eager().decode("ascii")

			if "invalid" in self.response:
				self.status = False
				self.message = "Username or password is not valid"
			else:
				self.status = True
				self.message = "Username or password is valid"

	def disconnect(self):
		if self.tn:
			self.tn.close()
			self.tn = None

	def runCommand(self):
		self.response = []
		OntDistance = ""
		OntRange = ""
		RxPower = ""
		TxPower = ""

		for i in range(0,len(self.data_cmd)):
			self.tn.write(telnetlib.IAC + telnetlib.WILL + telnetlib.ECHO)
			self.tn.write(self.data_cmd[i].encode("ascii")+b'\n')
			time.sleep(len(self.data_cmd[i])/10)

			# self.tn.read_until(b":")
			# self.tn.write(KEYS['EN'].encode("ascii")+b"\n")
			# time.sleep(3)
			# self.tn.read_until(b"OLT-TEST-WEB#")
			# data = self.tn.read_eager().decode("ascii")
			# print(data)
			# self.response.append(data)
			while True:
				data = self.tn.read_eager().decode("ascii").replace("'","").replace("\r","").split("\n")
				for x in range(0,len(data)):
					if data[x] != "":
						print(data[x])
						self.response.append(data[x])
					x=x+1
				if "return" in data[len(data)-1]:
					break
				elif "" in data[len(data)-1]:
					break
				else :
					self.tn.write(KEYS['EN'].encode("ascii")+b"\n")
					time.sleep(3)
			i=i+1
