"""
Views for the Api app
"""

# third-party imports
from ...helper.utils import response_json, make_request
from ...helper import constants
from ..models.Ont import Ont
from ..models.Host import Host
from flask_restful import Resource, reqparse

from pprint import pprint
from collections import Counter

import array as arr
import re, socket, logging, types, six, getpass, sys, telnetlib, getopt, ipaddress, os, time, datetime
from flaskthreads import AppContextThread
from threading import Thread

KEYS = {
	'UP': '\033[B',
	'DW': '\033[C',
	'EN': '\r'
}

parser = reqparse.RequestParser()  # pylint: disable=invalid-name
parser.add_argument('code' , type=str)

class OntAuotFind(Resource):
	code        	= None
	tn        		= None
	hostname        = None
	usr				= "rudytch"
	psw				= "Sementara2354"
	port            = 23
	hasecho         = False
	status			= True
	message			= None
	response		= None
	data_cmd		= None

	def get(self):
		args = parser.parse_args()
		self.code = args['code']
		self.response = []
		if self.code != None:
			result = Host(Code=self.code).show()
		else:
			result = Host().list_active()

		for rs in result:
			self.code = rs.Code
			self.hostname = rs.IpAddress
			self.usr = rs.Username
			self.psw = rs.Password
			if Ont(HostCode=self.code).count_by_host():
				Ont(HostCode=self.code).delete()
			print(self.code+": start Host")
			process = AppContextThread(target=self.function(self.code,self.hostname,self.usr,self.psw))
			process.start()
			process.join()
		return response_json(self.status, self.message, self.response)

	def function(self,code,hostname,usr,psw):
		self.connect(code,hostname,usr,psw)
		print("start running server")
		if self.tn:
			# self.tn.set_debuglevel(200)
			self.data_cmd = []
			self.data_cmd.append("enable")
			self.data_cmd.append("scroll 512")
			self.data_cmd.append("display ont autofind all")
			self.runCommand(code)
		self.disconnect()

	def connect(self,code,hostname,usr,psw):
		try:
			self.tn = telnetlib.Telnet(hostname,self.port)
		except socket.error:
			self.status = False
			self.message = "Problem connecting to host ('telnet %s %s')" % (self.hostname, self.port)

		if self.status:
			time.sleep(.2)
			self.tn.read_until(b"name:")
			self.tn.write(usr.encode("ascii")+b"\n")
			self.tn.read_until(b"password:")
			self.tn.write(psw.encode("ascii")+b"\n")
			time.sleep(.2)
			self.tn.write(b"y\n")
			self.tn.write(b"\n")
			time.sleep(.2)

			if "invalid" in self.response:
				self.status = False
				self.message = "Username or password is invalid"
			else:
				self.status = True
				self.message = "Username or password is valid"

	def disconnect(self):
		if self.tn:
			self.tn.close()
			self.tn = None

	def runCommand(self,code):
		prmNo = 0
		prmHostCode = ""
		prmSn = ""
		prmVersion = ""
		prmSoftwareVersion = ""
		prmEquipmentId = ""
		prmFrameId = ""
		prmSlotId = ""
		prmPortId = ""

		for i in range(0,len(self.data_cmd)):
			self.tn.write(telnetlib.IAC + telnetlib.WILL + telnetlib.ECHO)
			self.tn.write(self.data_cmd[i].encode("ascii")+b'\n')
			time.sleep(.2)
			self.tn.write(" ".encode("ascii")+b'\n')
			time.sleep(.2)
			self.tn.write(" ".encode("ascii")+b'\n')
			time.sleep(len(self.data_cmd)/10)
			last_data = ""
			while True:
				data = self.tn.read_very_eager().decode("ascii").replace("'","").replace("\r","").split("\n")
				ont_list = ["Number","Ont SN","Ont Version","F/S/P","Ont EquipmentID","Ont SoftwareVersion"]

				for x in range(0,len(data)):
					# print(data[x])
					if data[x] != "":
						prmHostCode = self.code
						if ont_list[0] in data[x]:
							prmNo = data[x].replace(ont_list[0],"").replace(":","").lstrip()
						if ont_list[1] in data[x]:
							prmSn = (data[x].replace(ont_list[1],"").replace(":","").lstrip().split())[0]
						if ont_list[2] in data[x]:
							prmVersion = data[x].replace(ont_list[2],"").replace(":","").lstrip()
						if ont_list[3] in data[x]:
							data_fps = data[x].replace(ont_list[4],"").lstrip()
							data_arr = data_fps.split(":")[1].split("/")
							prmFrameId = data_arr[0].lstrip()
							prmSlotId = data_arr[1].lstrip()
							prmPortId = data_arr[2].lstrip()
						if ont_list[4] in data[x]:
							prmEquipmentId = data[x].replace(ont_list[4],"").replace(":","").lstrip()
						if ont_list[5] in data[x]:
							prmSoftwareVersion = data[x].replace(ont_list[5],"").replace(":","").lstrip()

						if prmHostCode !="" and prmSn !="" and prmVersion !="" and prmSoftwareVersion !="" and prmEquipmentId !="" and prmFrameId !="" and prmSlotId !="" and prmPortId !="":
							if code != None:
								source = Ont(
									Code = prmSn+prmHostCode,
									HostCode = prmHostCode,
									Sn = prmSn,
									Version = prmVersion,
									SoftwareVersion = prmSoftwareVersion,
									EquipmentId = prmEquipmentId,
									FrameId = prmFrameId,
									SlotId = prmSlotId,
									PortId = prmPortId,
									ActiveStatus = 1
								)
								source.save()

							self.response.append([
								prmHostCode,
								prmSn,
								prmVersion,
								prmSoftwareVersion,
								prmEquipmentId,
								prmFrameId,
								prmSlotId,
								prmPortId
							])
							prmNo = 0
							prmHostCode = ""
							prmSn = ""
							prmVersion = ""
							prmSoftwareVersion = ""
							prmEquipmentId = ""
							prmFrameId = ""
							prmSlotId = ""
							prmPortId = ""

					x=x+1

				if "#" in data[len(data)-1] or "" in data[len(data)-1] or data == last_data:
					last_data = data
					break
				elif "More" in data[len(data)-1]:
					self.tn.write(" ".encode("ascii")+b"\n")
					time.sleep(.2)
				else :
					self.tn.write(" ".encode("ascii")+b"\n")
					time.sleep(.2)
