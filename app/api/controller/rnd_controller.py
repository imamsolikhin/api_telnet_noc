"""
Views for the Api app
"""

# third-party imports
from ...helper.utils import response_json, make_request
from ...helper import constants
from ..models.Host import Host
from flask_restful import Resource, reqparse

from pprint import pprint
from collections import Counter

import array as arr
import re, socket, logging, types, six, getpass, sys, telnetlib, getopt, ipaddress, os, time, datetime
from flaskthreads import AppContextThread

KEYS = {
	'UP': '\033[B',
	'DW': '\033[C',
	'EN': '\r'
}

parser = reqparse.RequestParser()  # pylint: disable=invalid-name
parser.add_argument('id', type=str)
parser.add_argument('code', type=str)


class RnD(Resource):
	id = None
	code = None
	status = None

	def get(self):
		args = parser.parse_args()
		self.id = args['id']
		self.code = args['code']
		thread = AppContextThread(target=self.sql(1))
		thread.start()
		thread.join()
		return response_json(self.status, self.code, self.id)

	def sql(self, x):
		result = Host(Code=self.code).show()
		for rs in result:
			self.id = rs.Code
		i = 1
		while i <= 100:
			i = i+1
