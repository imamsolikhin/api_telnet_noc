"""
Views for the Api app
"""

# third-party imports
from ...helper.utils import response_json, make_request
from ...helper import constants
from ..models.Clients import Clients
from ..models.Ont import Ont
from ..models.Host import Host
from ..models.OntInterface import OntInterface
from flask_restful import Resource, reqparse

from pprint import pprint
from collections import Counter

import array as arr
import re
import socket
import logging
import types
import six
import getpass
import sys
import telnetlib
import getopt
import ipaddress
import os
import time
import datetime
from flaskthreads import AppContextThread

KEYS = {
	'UP': '\033[B',
	'DW': '\033[C',
	'EN': '\r'
}

parser = reqparse.RequestParser()  # pylint: disable=invalid-name
parser.add_argument('sn', type=str)
parser.add_argument('code', type=str)


class CheckSignal(Resource):
	code = None
	tn = None
	ip = None
	hostname = None
	sn = None
	usr = None
	psw = None
	port = 23
	hasecho = False
	tmout = 15
	status = True
	message = None
	response = None
	data_cmd = None

	id = None
	f = None
	s = None
	p = None

	def get(self):
		args = parser.parse_args()
		self.sn = args['sn']
		self.code = args['code']
		thread = AppContextThread(target=self.function())
		thread.start()
		thread.join()
		return response_json(self.status, self.message, self.response)

	def function(self):
		rest_clients = Clients(Code=self.code).show()
		for row_clients in rest_clients:
			if row_clients.OntId:
				rest_ont = OntInterface(OntSn=row_clients.OntSn).show_bysn()
				if rest_ont:
					for row_onts in rest_ont:
						self.sn = str(row_onts.OntSn)
						self.id = str(row_onts.OntId)
						self.f = str(row_onts.FrameId)
						self.s = str(row_onts.SlotId)
						self.p = str(row_onts.PortId)
				else:
					self.sn = str(row_clients.OntSn)
					self.id = str(row_clients.OntId)
					self.f = str(row_clients.FrameId)
					self.s = str(row_clients.SlotId)
					self.p = str(row_clients.PortId)

				rest_host = Host(Code=row_clients.HostCode).show()
				for row_host in rest_host:
					self.ip = str(row_host.IpAddress)
					self.hostname = str(row_host.Hostname)
					self.usr = str(row_host.Username)
					self.psw = str(row_host.Password)
					self.port = row_host.Port
			else:
				rest_ont = Ont(Sn=self.sn).show_sn()
				for row_ont in rest_ont:
					self.sn = str(row_ont.Sn)
					self.id = "127"
					self.f = str(row_ont.FrameId)
					self.s = str(row_ont.SlotId)
					self.p = str(row_ont.PortId)
					rest_host = Host(Code=row_ont.HostCode).show()
					for row_host in rest_host:
						self.ip = str(row_host.IpAddress)
						self.usr = str(row_host.Username)
						self.psw = str(row_host.Password)
						self.port = row_host.Port
		else:
			rest_ont = Ont(Sn=self.sn).show_sn()
			for row_ont in rest_ont:
				self.sn = str(row_ont.Sn)
				self.id = "127"
				self.f = str(row_ont.FrameId)
				self.s = str(row_ont.SlotId)
				self.p = str(row_ont.PortId)
				rest_host = Host(Code=row_ont.HostCode).show()
				for row_host in rest_host:
					self.ip = str(row_host.IpAddress)
					self.usr = str(row_host.Username)
					self.psw = str(row_host.Password)
					self.port = row_host.Port

		self.connect()
		if self.tn:
			# check Current Config OLT
			# self.tn.set_debuglevel(200)
			self.data_cmd = []
			self.data_cmd.append('enable')
			self.data_cmd.append('scroll 512')
			self.data_cmd.append('config')
			# self.data_cmd.append('display ')
			self.data_cmd.append('interface gpon '+str(self.f)+'/'+str(self.s))

			if rest_clients:
				for row_clients in rest_clients:
					if row_clients.OntId:
						self.data_cmd.append('display ont info '+str(self.p)+' '+str(self.id))
						self.data_cmd.append('display ont optical-info '+str(self.p)+' '+str(self.id))
						self.data_cmd.append('quit')
					else:
						self.data_cmd.append('ont add '+str(self.p)+ ' '+str(self.id)+' sn-auth "'+str(self.sn)+'" omci desc "Add Test"')
						self.data_cmd.append('display ont info '+str(self.p)+' '+str(self.id))
						self.data_cmd.append('display ont optical-info '+str(self.p)+' '+str(self.id))
						self.data_cmd.append('ont delete '+str(self.p)+' '+str(self.id))
						self.data_cmd.append('quit')
			else:
				self.data_cmd.append('ont add '+str(self.p)+ ' '+str(self.id)+' sn-auth "'+str(self.sn)+'" omci desc "Add Test"')
				self.data_cmd.append('display ont info '+str(self.p)+' '+str(self.id))
				self.data_cmd.append('display ont optical-info '+str(self.p)+' '+str(self.id))
				self.data_cmd.append('ont delete '+str(self.p)+' '+str(self.id))
				self.data_cmd.append('quit')

			self.runCommand()

		self.disconnect()

	def connect(self):
		try:
			self.tn = telnetlib.Telnet(self.ip, self.port)
		except socket.error:
			self.status = False
			self.message = "Problem connecting to signal ('telnet %s %s')" % (
				self.ip, self.port)

		if self.status:
			time.sleep(.2)
			self.tn.read_until(b"name:")
			self.tn.write(self.usr.encode("ascii")+b"\n")
			self.tn.read_until(b"password:")
			self.tn.write(self.psw.encode("ascii")+b"\n")
			time.sleep(.2)
			self.tn.write(b"y\n")
			self.tn.write(b"\n")
			time.sleep(.2)

			self.response = self.tn.read_very_eager().decode("ascii")

			if "invalid" in self.response:
				self.status = False
				self.message = "Username or password is invalid"
			else:
				self.status = True
				self.message = "Username or password is valid"

	def disconnect(self):
		if self.tn:
			self.tn.close()
			self.tn = None

	def runCommand(self):
		self.response = []
		OntDistance = ""
		OntRange = ""
		RxPower = ""
		TxPower = ""

		for i in range(0, len(self.data_cmd)):
			self.tn.write(telnetlib.IAC + telnetlib.WILL + telnetlib.ECHO)
			self.tn.write(self.data_cmd[i].encode("ascii")+b'\n')
			time.sleep(len(self.data_cmd[i])/7)
			while True:
				data = self.tn.read_very_eager().decode("ascii").replace("'", "").replace("\r", "").split("\n")
				data = [y for y in data if y != []]
				ont_list = ["Rx optical power(dBm)", "Tx optical power(dBm)","ONT distance(m)", "Rx optical power(dBm)"]

				for x in range(0, len(data)):
					print(data[x])
					if data[x] != "":
						if ont_list[0] in data[x]:
							RxPower = data[x].replace(ont_list[0], "").replace(":", "").lstrip()
						if ont_list[1] in data[x]:
							TxPower = data[x].replace(ont_list[1], "").replace(":", "").lstrip()
						if ont_list[2] in data[x]:
							OntDistance = data[x].replace(ont_list[2], "").replace(":", "").lstrip()
						if ont_list[3] in data[x]:
							OntRange = data[x].replace(ont_list[3], "").replace(":", "").lstrip()

						if RxPower != "" and TxPower != "" and OntDistance != "" and OntRange != "":
							self.response.append([
								TxPower,
								RxPower,
								OntDistance,
								OntRange,
								self.hostname,
								self.ip
							])
							RxPower = ""
							OntDistance = ""

					x = x+1

				if "#" in data[len(data)-1]:
					break
				elif "" in data[len(data)-1]:
					self.tn.write(KEYS['EN'].encode("ascii")+b"\n")
				elif "More" in data[len(data)-1]:
					self.tn.write(KEYS['EN'].encode("ascii")+b"\n")
				elif "Failure" in data[len(data)-1]:
					self.message = str(self.sn)+" "+data[x].replace("Failure:", "")
				else:
					self.tn.write(KEYS['EN'].encode("ascii")+b"\n")
				i = i+1
