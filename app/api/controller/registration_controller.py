"""
Views for the Api app
"""

# third-party imports
from ...helper.utils import response_json, make_request
from ...helper import constants
from ..service.ActivationService import ActivationService
from ..models.Clients import Clients
from ..models.Customer import Customer
from ..models.ClientVlanService import ClientVlanService
from ..models.Ont import Ont
from ..models.Host import Host
from ..models.GemProfile import GemProfile
from ..models.ServicePort import ServicePort
from ..models.OntInterface import OntInterface
from ..models.HostTrafficTable import HostTrafficTable
from ..models.HostSrvProfile import HostSrvProfile
from ..models.HostLineProfile import HostLineProfile
from ..models.PortVlan import PortVlan
from flask_restful import Resource, reqparse

from pprint import pprint
from collections import Counter
from json import JSONEncoder

import array as arr
import re
import socket
import sys
import telnetlib
import os
import time
import datetime
from flaskthreads import AppContextThread

KEYS = {
	'UP': '\033[B',
	'DW': '\033[C',
	'EN': '\r'
}

parser = reqparse.RequestParser()  # pylint: disable=invalid-name
parser.add_argument('code', type=str)
parser.add_argument('config', type=str)
parser.add_argument('modify_sn', type=str)


class Activation(Resource):
	config = None
	code = None
	modify_sn = None
	tn = None
	ip = None
	usr = None
	psw = None
	port = None
	hasecho = False
	tmout = 15
	status = True
	message = None
	response = None
	data_cmd = None

	flag = False
	c_code = None
	sn_id = None
	vln_set = None
	m_host = None
	m_clients = None
	m_customer = None
	m_isp = None
	m_client_service = []

	def get(self):
		args = parser.parse_args()
		self.c_code = args['code']
		self.config = args['config']
		self.modify_sn = args['modify_sn']
		rest_clients = Clients(Code=self.c_code).show()
		resp = Clients(Code=self.c_code).show()
		if resp:
			self.m_clients = resp[0]
			if self.m_clients.OntId:
				self.sn_id = self.m_clients.OntId
			else:
				self.sn_id = self.getIdxOnt()
			self.sn_id = self.getIdxOnt()
			if self.m_clients.VlanAttribut == "QinQ":
				self.vln_set = "eth"
			else:
				self.vln_set = "vlan"

		resp = Host(Code=self.m_clients.HostCode).show()
		if resp:
			self.m_host = resp[0]
			self.ip = str(self.m_host.IpAddress)
			self.usr = str(self.m_host.Username)
			self.psw = str(self.m_host.Password)
			self.port = self.m_host.Port
		resp = Customer(Code=self.m_clients.CustomerCode).show()
		if resp:
			self.m_customer = resp[0]
		resp = Customer(Code=self.m_customer.LinkID).show()
		if resp:
			self.m_isp = resp[0]
		self.m_client_service = ClientVlanService(CustomerCode=self.c_code).show()

		self.data_cmd = []
		self.data_cmd.append('enable')
		self.data_cmd.append('scroll 512')
		self.data_cmd.append('config')
		self.connect()

		if self.config == "Active" or self.config == "Active-script":
			#OLT Traffict Table
			self.response = []
			for row in self.m_client_service:
				if row.TrafficTable:
					resp = HostTrafficTable(HostCode=self.m_clients.HostCode,Index=row.TrafficTable).count_idx()
					if resp == 0:
						self.data_cmd.append('traffic table ip index '+str(row.TrafficTable)+' cir '+str(int(row.TrafficTable)*1024)+' pir '+str(int(row.TrafficTable)*1024)+' priority 0 priority-policy local-setting')

				resp = PortVlan(HostCode=self.m_clients.HostCode,Index=row.Vlan).count_idx()
				if resp == 0:
					self.data_cmd.append('vlan '+str(row.Vlan)+' smart')
					resp = PortVlan(
								HostCode=self.m_clients.HostCode,
								Frame=self.m_clients.VlanFrameId,
								Slot=self.m_clients.VlanSlotId,
								Port=self.m_clients.VlanPortId,
								Index=row.Vlan
							).count_idx_port()
					if resp == 0:
						self.data_cmd.append('port vlan '+str(row.Vlan)+ ' '+str(self.m_clients.VlanFrameId)+'/'+str(self.m_clients.VlanSlotId)+' '+str(self.m_clients.VlanPortId))

				if self.m_clients.VlanAttribut != "Standard":
					resp = PortVlan(
							HostCode=self.m_clients.HostCode,
							Frame=self.m_clients.VlanFrameId,
							Slot=self.m_clients.VlanSlotId,
							Port=self.m_clients.VlanPortId,
							Attrib="stacking",
							Index=row.Vlan
							).count_idx_attr()
					if resp == 0 or resp == None or resp == "":
						self.data_cmd.append('vlan attrib '+str(row.Vlan)+' stacking')

			#OLT Service Profile
			resp = HostSrvProfile(HostCode=self.m_clients.HostCode,ProfileId=self.m_clients.OntSrvProfileId).count_idx()
			if resp == 0:
				self.data_cmd.append('ont-srvprofile gpon profile-id '+str(self.m_clients.OntSrvProfileId))
				self.data_cmd.append('ont-port pots adaptive eth adaptive')
				if self.m_clients.NativeVlanEth1 != "None" and self.m_clients.NativeVlanEth1 != "" and self.m_clients.NativeVlanEth1 != None:
					self.data_cmd.append('port vlan eth 1 transparent')
				if self.m_clients.NativeVlanEth2 != "None" and self.m_clients.NativeVlanEth2 != "" and self.m_clients.NativeVlanEth2 != None:
					self.data_cmd.append('port vlan eth 2 transparent')
				if self.m_clients.NativeVlanEth3 != "None" and self.m_clients.NativeVlanEth3 != "" and self.m_clients.NativeVlanEth3 != None:
					self.data_cmd.append('port vlan eth 3 transparent')
				if self.m_clients.NativeVlanEth4 != "None" and self.m_clients.NativeVlanEth4 != "" and self.m_clients.NativeVlanEth4 != None:
					self.data_cmd.append('port vlan eth 4 transparent')
				self.data_cmd.append('commit')
				self.data_cmd.append('quit')

			#OLT Line Profile
			self.data_cmd.append('ont-lineprofile gpon profile-id '+str(self.m_clients.OntLineProfileId))
			if self.m_clients.VlanAttribut == "QinQ":
				self.data_cmd.append('mapping-mode port')

			self.data_cmd.append('tcont 1 dba-profile-id 10')
			vlan_id = 0
			eth_i = 0
			resp = 0
			for row in self.m_client_service:
				resp_vlan = GemProfile(HostCode=self.m_clients.HostCode, ProfileId=self.m_clients.OntLineProfileId,GemVlan=row.UserVlan).count_by_vlan()
				if resp_vlan == 0 and vlan_id != row.UserVlan:
					vlan_id = row.UserVlan
					eth_i = eth_i+1
					resp = ActivationService.getGemByVlan(self, self.m_clients.HostCode, self.m_clients.OntLineProfileId, row.UserVlan, resp)
					self.data_cmd.append('gem add '+str(resp)+' eth tcont 1')
					if self.m_clients.VlanAttribut == "QinQ":
						self.data_cmd.append('gem mapping '+str(resp)+ ' 0 '+str(self.vln_set)+' '+str(eth_i))
					else:
						self.data_cmd.append('gem mapping '+str(resp)+ ' 0 '+str(self.vln_set)+' '+str(row.UserVlan))
					resp = resp+1
			if eth_i != 0:
				self.data_cmd.append('commit')

			self.data_cmd.append('quit')

			# Activasi Customer
			self.data_cmd.append('interface gpon '+str(self.m_clients.FrameId)+'/'+str(self.m_clients.SlotId))
			if self.m_clients.OntId != None or self.m_clients.OntId != "" or self.m_clients.OntSn != None or self.m_clients.OntSn != "":
				resp = OntInterface(
						HostCode=self.m_clients.HostCode,
						FrameId=self.m_clients.FrameId,
						SlotId=self.m_clients.SlotId,
						PortId=self.m_clients.PortId,
						OntSn=self.m_clients.OntSn
					  ).count_sn()
				if resp == 0:
				   self.flag = False

				resp = OntInterface(
						HostCode=self.m_clients.HostCode,
						FrameId=self.m_clients.FrameId,
						SlotId=self.m_clients.SlotId,
						PortId=self.m_clients.PortId,
						OntSn=self.m_clients.OntSn,
						Flag="deactivated"
					  ).count_flag()
				if resp != 0:
				   self.flag = True

			if self.flag:
				self.data_cmd.append('ont activate '+str(self.m_clients.PortId)+' '+str(self.m_clients.OntId))
			else:
				if self.m_clients.OntId == None:
					self.data_cmd.append('ont add '+str(self.m_clients.PortId)+' '+str(self.sn_id)+' sn-auth "'+str(self.m_clients.OntSn)+'" omci ont-lineprofile-id '+ str(self.m_clients.OntLineProfileId)+' ont-srvprofile-id '+str(self.m_clients.OntSrvProfileId)+' desc "'+str(str(self.m_isp.Name))+' '+str(self.m_customer.Name)+'"')

					# Setting Native Vlan
					if self.m_clients.NativeVlanEth1 != "None" and self.m_clients.NativeVlanEth1 != "" and self.m_clients.NativeVlanEth1 != None:
						self.data_cmd.append('ont port native-vlan '+str(self.m_host.PortId)+' '+ str(self.sn_id)+' eth 1 vlan '+str(self.m_clients.NativeVlanEth1)+' priority 0')
					if self.m_clients.NativeVlanEth2 != "None" and self.m_clients.NativeVlanEth2 != "" and self.m_clients.NativeVlanEth2 != None:
						self.data_cmd.append('ont port native-vlan '+str(self.m_host.PortId)+' '+ str(self.sn_id)+' eth 2 vlan '+str(self.m_clients.NativeVlanEth2)+' priority 0')
					if self.m_clients.NativeVlanEth3 != "None" and self.m_clients.NativeVlanEth3 != "" and self.m_clients.NativeVlanEth3 != None:
						self.data_cmd.append('ont port native-vlan '+str(self.m_host.PortId)+' '+ str(self.sn_id)+' eth 3 vlan '+str(self.m_clients.NativeVlanEth3)+' priority 0')
					if self.m_clients.NativeVlanEth4 != "None" and self.m_clients.NativeVlanEth4 != "" and self.m_clients.NativeVlanEth4 != None:
						self.data_cmd.append('ont port native-vlan '+str(self.m_host.PortId)+' '+ str(self.sn_id)+' eth 4 vlan '+str(self.m_clients.NativeVlanEth4)+' priority 0')
			self.data_cmd.append('quit')

			# Activasi Service Customer
			vlan_id = 0
			resp_gem = 0
			srv_id = 0
			for row in self.m_client_service:
				if row.ServicePortId != None:
					srv_id = row.ServicePortId
				else:
					srv_id = self.getIdxSrvPort(srv_id)

					if vlan_id != row.UserVlan:
						vlan_id = row.UserVlan
						resp_gem = ActivationService.getGemByVlan(self, self.m_clients.HostCode, self.m_clients.OntLineProfileId, row.UserVlan, resp_gem)

					trafficttable=""
					innervlan=""
					if row.TrafficTable:
						trafficttable ='inbound traffic-table index '+str(row.TrafficTable)+' outbound traffic-table index '+str(row.TrafficTable)
					if row.InnerVlan:
						innervlan ='inner-vlan '+str(row.InnerVlan)
					if row.ServicePortId == None:
						self.data_cmd.append('service-port '+str(srv_id)+' vlan '+str(row.Vlan)+' gpon '+str(self.m_clients.FrameId)+'/'+str(self.m_clients.SlotId)+'/'+str(self.m_clients.PortId)+' ont '+str(self.sn_id)+' gemport '+str(resp_gem)+ ' multi-service user-vlan '+str(row.UserVlan)+ ' tag-transform '+row.TagTransform+' '+innervlan+ ' '+trafficttable)

					resp_gem = int(resp_gem)+1
					srv_id = int(srv_id)+1

			if self.config == "Active":
				self.response = []
				if self.tn:
					# self.tn.set_debuglevel(200)
					result = self.runCommand()
					if self.flag:
						Clients(Code=self.c_code, OntSn=self.m_clients.OntSn, OntId=str(self.m_clients.OntId), Response=result).update_idx()
					else:
						if self.m_clients.OntId == None:
							Clients(Code=self.c_code, OntSn=self.m_clients.OntSn, OntId=str(self.sn_id), Response=result).update_idx()

					vlan_id = 0
					resp_gem = 0
					srv_id = 0
					for row in self.m_client_service:
						if row.ServicePortId != None:
							srv_id = row.ServicePortId
						else:
							srv_id = self.getIdxSrvPort(srv_id)

							if vlan_id != row.UserVlan:
								vlan_id = row.UserVlan
								resp_gem =ActivationService.getGemByVlan(self, self.m_clients.HostCode, self.m_clients.OntLineProfileId, row.UserVlan, resp_gem)
							if row.GemPort != None:
								ClientVlanService(Code=row.Code, ServicePortId=srv_id, GemPort=row.GemPort).update()
							else:
								ClientVlanService(Code=row.Code, ServicePortId=srv_id, GemPort=resp_gem).update()

							resp_gem = int(resp_gem)+1
							srv_id = int(srv_id)+1
				self.disconnect()

		elif self.config == "Postpone" or self.config == "Postpone-script":
			self.response = []
			if self.tn:
				# self.tn.set_debuglevel(200)
				self.data_cmd.append('interface gpon '+str(self.m_clients.FrameId)+'/'+str(self.m_clients.SlotId))
				self.data_cmd.append('ont deactivate '+str(self.m_clients.PortId)+' '+str(self.m_clients.OntId))

				if self.config == "Postpone":
					result = self.runCommand()
					Clients(Code=self.c_code, OntSn=self.m_clients.OntSn, OntId=self.m_clients.OntId, Response=result).update_idx()

		elif self.config == "Closed" or self.config == "Closed-script":
			self.response = []
			if self.tn:
				# self.tn.set_debuglevel(200)
				for row in self.m_client_service:
					self.data_cmd.append('undo service-port '+str(row.ServicePortId))

				self.data_cmd.append('interface gpon '+str(self.m_clients.FrameId)+'/'+str(self.m_clients.SlotId))
				self.data_cmd.append('ont delete '+str(self.m_clients.PortId)+' '+str(self.m_clients.OntId))

				if self.config == "Closed":
					result = self.runCommand()
					Clients(Code=self.c_code, Response=result).update_idx()
					for row in self.m_client_service:
						ClientVlanService(Code=row.Code).update()

		elif self.config == "Modify" or self.config == "Modify-script":
			self.response = []
			if self.tn:
				self.data_cmd.append('interface gpon '+str(self.m_clients.FrameId)+'/'+str(self.m_clients.SlotId))
				self.data_cmd.append('ont modify '+str(self.m_clients.PortId)+' '+str(self.m_clients.OntId) +' sn '+ str(self.modify_sn))

				if self.config == "Modify":
					result = self.runCommand()
					Clients(Code=self.c_code, OntSn=self.modify_sn, Response=result).update_sn()

		self.disconnect()
		return response_json(self.status, self.message, self.data_cmd)

	def connect(self):
		try:
			self.tn = telnetlib.Telnet(self.ip, self.port)
		except socket.error:
			self.status = False
			self.message = "Problem connecting to active ('telnet %s %s')" % (self.ip, self.port)

		if self.status:
			time.sleep(.2)
			self.tn.read_until(b"name:")
			self.tn.write(self.usr.encode("ascii")+b"\n")
			self.tn.read_until(b"password:")
			self.tn.write(self.psw.encode("ascii")+b"\n")
			time.sleep(.2)
			self.tn.write(b"y\n")
			self.tn.write(b"\n")
			time.sleep(.2)

			self.response = self.tn.read_very_eager().decode("ascii")

			if "invalid" in self.response:
				self.status = False
				self.message = "Username or password is invalid"
			else:
				self.status = True
				self.message = "Username or password is valid"

	def disconnect(self):
		if self.tn:
			self.tn.close()
			self.tn = None

	def runCommand(self):
		resp = []
		for i in range(0, len(self.data_cmd)):
			print(self.data_cmd[i])
			resp.append("command : "+str(self.data_cmd[i]))
			self.tn.write(telnetlib.IAC + telnetlib.WILL + telnetlib.ECHO)
			self.tn.write(self.data_cmd[i].encode("ascii")+b'\n')
			time.sleep(len(self.data_cmd[i])/25)
			# self.tn.read_until(b":",timeout=.3)
			self.tn.write(" ".encode("ascii")+b"\n")
			self.tn.write(" ".encode("ascii")+b"\n")
			while True:
				data = self.tn.read_very_eager().decode("ascii").replace("'", "").replace("\r", "").replace("\u0007", "").split("\n")
				for x in range(0, len(data)):
					if data[x].replace(" ", "") != "" and data[x].replace(" ", "") != "#":
						print(data[x])
						resp.append(data[x])

				if "#" in data[len(data)-1]:
					break
				elif "error" in data[len(data)-1]:
					break
				elif "Failure:" in data[len(data)-1]:
					break
				elif ":" in data[len(data)-1]:
					self.tn.write(" ".encode("ascii")+b"\n")
					time.sleep(.2)
				elif "" in data[len(data)-1]:
					self.tn.write(" ".encode("ascii")+b"\n")
					time.sleep(.2)
				elif "Failure" in data[len(data)-1]:
					self.tn.write(" ".encode("ascii")+b"\n")
					time.sleep(.2)
				elif "More" in data[len(data)-1]:
					self.tn.write(" ".encode("ascii")+b"\n")
					time.sleep(.2)
				else:
					self.tn.write(" ".encode("ascii")+b"\n")
					time.sleep(.2)
				i = i+1

		result = "\n".join(resp)
		return result

	def getIdxOnt(self):
		idx = 0
		while True:
			resp = OntInterface(
					HostCode=self.m_clients.HostCode,
					FrameId=self.m_clients.FrameId,
					SlotId=self.m_clients.SlotId,
					PortId=self.m_clients.PortId,
					OntId=idx
					).count_idx()

			if resp == 0:
				break
			idx = idx+1
		return idx

	def getIdxGem(self):
		idx = 0
		while True:
			resp = GemProfile(
					HostCode=self.m_clients.HostCode,
					ProfileId=self.m_clients.OntLineProfileId,
					GemId=idx
					).count_by_id()

			if resp == 0:
				break
			idx = idx+1
		return idx

	def getSrvGem(self):
		idx = 0
		while True:
			resp = ServicePort(HostCode=self.m_clients.HostCode).count()
			if resp == 0:
				break
			idx = idx+1
		return idx

	def getIdxGemVlan(self,vlan):
		resp = GemProfile(
					HostCode=self.m_clients.HostCode,
					ProfileId=self.m_clients.OntLineProfileId,
					GemVlan=vlan
					).count_by_vlan()
		return resp

	def getIdxVlan(self,vlan):
		resp = GemProfile(
					HostCode=self.m_clients.HostCode,
					ProfileId=self.m_clients.OntLineProfileId,
					GemVlan=vlan
					).count_by_vlan()
		return resp

	def getServiceVlan(self,vlan):

		resp = ServicePort(
					HostCode=self.m_clients.HostCode,
					GemVlan=vlan
					).count_by_vlan()
		if resp == 0:
			return resp
		else:

			return

	def getIdxSrvPort(self,idx_i):
		idx = idx_i
		while True:
			resp = ServicePort(
					HostCode=self.m_clients.HostCode,
					Index=idx
					).count_by_idx()

			if resp == 0:
				break
			idx = idx+1
		return idx
