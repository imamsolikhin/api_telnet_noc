"""
Views for the Api app
"""

# third-party imports
from ...helper.utils import response_json, make_request
from ...helper import constants
from ..models.Clients import Clients
from ..models.ClientsTools import ClientsTools
from ..models.Host import Host
from ..models.HostLineProfile import HostLineProfile
from ..models.HostLineProfileLast import HostLineProfileLast
from ..models.HostSrvProfile import HostSrvProfile
from ..models.HostSrvProfileLast import HostSrvProfileLast
from ..models.HostTrafficTable import HostTrafficTable
from ..models.HostTrafficTableLast import HostTrafficTableLast
from ..models.DbaProfile import DbaProfile
from ..models.DbaProfileLast import DbaProfileLast
from ..models.PortVlan import PortVlan
from ..models.PortVlanLast import PortVlanLast
from ..models.GemProfile import GemProfile
from ..models.GemProfileLast import GemProfileLast
from ..models.ServicePort import ServicePort
from ..models.ServicePortLast import ServicePortLast
from ..models.OntInterface import OntInterface
from ..models.OntInterfaceLast import OntInterfaceLast
from ..models.Ont import Ont
from ..models.OntLast import OntLast
from flask_restful import Resource, reqparse

from pprint import pprint
from collections import Counter

import array as arr
import re
import socket
import logging
import types
import six
import getpass
import sys
import telnetlib
import getopt
import ipaddress
import os
import time
import datetime
from flaskthreads import AppContextThread

KEYS = {
	'UP': '\033[B' ,
	'DW': '\033[C' ,
	'EN': '\r'
}

parser = reqparse.RequestParser()  # pylint: disable=invalid-name
parser.add_argument('code' , type=str)
parser.add_argument('config' , type=str)


class CheckClientsTools(Resource):
	code = None
	config = None
	tn = None
	IpAddress = None
	Hostname = None
	usr = None
	psw = None
	port = None
	hasecho = False
	status = True
	message = None
	response = None
	data_cmd = None
	res_clients = None
	line_profile = []

	def get(self):
		args = parser.parse_args()
		self.code = args['code']
		self.config = args['config']
		thread = AppContextThread(target=self.function())
		thread.start()
		thread.join()
		return response_json(self.status, self.message, self.response)

	def function(self):
		self.res_clients = Clients(Code=self.code).show()
		for row_clients in self.res_clients:
			self.code = row_clients.Code
			result = Host(Code=row_clients.HostCode).show()
			for rs in result:
				self.IpAddress = rs.IpAddress
				self.Hostname = rs.Hostname
				self.usr = rs.Username
				self.psw = rs.Password
				self.port = rs.Port

		self.connect()

		# self.tn.set_debuglevel(200)
		self.data_cmd = []
		self.data_cmd.append('enable')
		self.data_cmd.append('scroll 512')
		self.data_cmd.append('config')
		self.runCommand()

		self.response = []

		if self.config != None:
			for row_clients in self.res_clients:
				rs = ClientsTools(Code=self.code).count()
				if int(rs)==0:
					ClientsTools(Code=self.code,ClientsCode=self.code,ActiveStatus=1).save()

				if self.config=='ont-version' or self.config =='all':
					self.response = []
					self.runCommandResp('display ont version '+str(row_clients.FrameId)+' '+str(row_clients.SlotId)+' '+str(row_clients.PortId)+' '+str(row_clients.OntId)+'')
					result = "\n".join(self.response)
					ClientsTools(Code=self.code, Version=result).update_Version()

				if self.config=='ont-info' or self.config =='all':
					self.response = []
					self.runCommandResp('display ont info '+str(row_clients.FrameId)+' '+str(row_clients.SlotId)+' '+str(row_clients.PortId)+' '+str(row_clients.OntId)+'')
					result = "\n".join(self.response)
					ClientsTools(Code=self.code, OntInfo=result).update_OntInfo()

				if self.config =='port-state' or self.config == 'all':
					self.response = []
					self.runCommandResp('interface gpon '+str(row_clients.FrameId)+'/'+str(row_clients.SlotId))
					self.runCommandResp('display ont port state '+str(row_clients.PortId)+' '+str(row_clients.OntId)+' eth-port all')
					self.runCommandResp('quit')
					result = "\n".join(self.response)
					ClientsTools(Code=self.code, PortState=result).update_PortState()

				if self.config =='register-info' or self.config =='all':
					self.response = []
					self.runCommandResp('interface gpon '+str(row_clients.FrameId)+'/'+str(row_clients.SlotId))
					self.runCommandResp('display ont register-info '+str(row_clients.PortId)+' '+str(row_clients.OntId)+' ')
					self.runCommandResp('quit')
					result = "\n".join(self.response)
					ClientsTools(Code=self.code, Registerinfo=result).update_Registerinfo()

				if self.config =='wan-info' or self.config =='all':
					self.response = []
					self.runCommandResp('display ont wan-info '+str(row_clients.FrameId)+'/'+str(row_clients.SlotId)+' '+str(row_clients.PortId)+' '+str(row_clients.OntId))
					result = "\n".join(self.response)
					ClientsTools(Code=self.code, WanInfo=result).update_WanInfo()

				if self.config =='mac-address' or self.config =='all':
					self.response = []
					self.runCommandResp('display mac-address port '+str(row_clients.FrameId)+'/'+str(row_clients.SlotId)+'/'+str(row_clients.PortId)+' ont '+str(row_clients.OntId))
					result = "\n".join(self.response)
					ClientsTools(Code=self.code, MacAddress=result).update_MacAddress()

				if self.config =='port-attribute' or self.config =='all':
					self.response = []
					self.runCommandResp('interface gpon '+str(row_clients.FrameId)+'/'+str(row_clients.SlotId))
					self.runCommandResp('display ont port attribute '+str(row_clients.PortId)+' '+str(row_clients.OntId)+' eth')
					self.runCommandResp('quit')
					result = "\n".join(self.response)
					ClientsTools(Code=self.code, PortAttribute=result).update_PortAttribute()

				if self.config =='optical-info' or self.config =='all':
					self.response = []
					self.runCommandResp('interface gpon '+str(row_clients.FrameId)+'/'+str(row_clients.SlotId))
					self.runCommandResp('display ont optical-info '+str(row_clients.PortId)+' '+str(row_clients.OntId)+'')
					self.runCommandResp('quit')
					result = "\n".join(self.response)
					ClientsTools(Code=self.code, Opticalinfo=result).update_Opticalinfo()

				if self.config =='fec-crc-error' or self.config =='all':
					self.response = []
					self.runCommandResp('interface gpon '+str(row_clients.FrameId)+'/'+str(row_clients.SlotId))
					self.runCommandResp('port '+str(row_clients.PortId)+' fec enable')
					self.runCommandResp('display statistics ont-line-quality '+str(row_clients.PortId)+' '+str(row_clients.OntId))
					self.runCommandResp('quit')
					result = "\n".join(self.response)
					ClientsTools(Code=self.code, FecCrcError=result).update_FecCrcError()

		self.disconnect()

	def connect(self):
		try:
			self.tn = telnetlib.Telnet(self.IpAddress, self.port)
		except socket.error:
			self.status = False
			self.message = "Problem connecting to host ('telnet %s %s')" % (self.IpAddress, self.port)

		if self.status:
			time.sleep(.2)
			self.tn.read_until(b"name:")
			self.tn.write(self.usr.encode("ascii")+b"\n")
			self.tn.read_until(b"password:")
			self.tn.write(self.psw.encode("ascii")+b"\n")
			time.sleep(.2)
			self.tn.write(b"y\n")
			self.tn.write(b"\n")
			time.sleep(.2)

			self.response = self.tn.read_very_eager().decode("ascii")

			if "invalid" in self.response:
				self.status = False
				self.message = "Username or password is not valid"
			else:
				self.status = True
				self.message = "Username or password is valid"

	def disconnect(self):
		if self.tn:
			self.tn.close()
			self.tn = None

	def runCommand(self):
		self.response = []
		OntDistance = ""
		OntRange = ""
		RxPower = ""
		TxPower = ""

		for i in range(0, len(self.data_cmd)):
			self.tn.write(telnetlib.IAC + telnetlib.WILL + telnetlib.ECHO)
			self.tn.write(self.data_cmd[i].encode("ascii")+b'\n')
			time.sleep(len(self.data_cmd[i])/15)
			while True:
				data = self.tn.read_very_eager().decode("ascii").replace("'", "").replace("\r", "").split("\n")
				if "#" in data[len(data)-1]:
					break
				elif "" in data[len(data)-1]:
					self.tn.write(" ".encode("ascii")+b"\n")
					time.sleep(0.2)
				elif "More" in data[len(data)-1] or "}:" in data[len(data)-1].replace(" ", ""):
					self.tn.write(" ".encode("ascii")+b"\n")
					time.sleep(0.2)
				else:
					self.tn.write(" ".encode("ascii")+b"\n")
					time.sleep(0.2)
				i = i+1

	def runCommandResp(self, cmd):
		self.tn.write(cmd.encode("ascii")+b"\n")
		time.sleep(len(cmd)/15)
		data = self.tn.read_very_eager().decode("ascii").replace("\r", "").split("\n")
		x=0
		dt_l = len(data)
		while True:
			if len(data) == (x+1):
				break
			if data[x].replace(" ", "") != "":
				if data[x].replace(" ", "") != "#":
					self.response.append(data[x].replace("<", "[").replace(">", "]").replace("---- More ( Press 'Q' to break ) ----",""))
					if " Press 'Q' to break " in data[x] or "}:" in data[len(data)-1].replace(" ", ""):
						self.response.append(data[x].replace("<", "[").replace(">", "]").replace("---- More ( Press 'Q' to break ) ----",""))
						self.tn.write(" ".encode("ascii")+b"\n")
						time.sleep(2)
						data = self.tn.read_very_eager().decode("ascii").replace("\r", "").split("\n")
						dt_l = len(data)
						x = 0
			x=x+1
