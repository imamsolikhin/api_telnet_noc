"""
Models for the Api app
"""

# local imports
from ...config.db import Base, db_session
from sqlalchemy import Column, Integer, String, DateTime, Text, Numeric, func
from sqlalchemy.dialects.mysql import LONGTEXT

class Clients(Base):
	"""
	This class represents the clients table.
	"""

	# Ensures table will be named in plural and not in singular as in
	# the name of the model
	__tablename__ = 'mst_customer_noc'

	Code = Column(String(length=250), primary_key=True)
	HostCode = Column(String(length=250))
	CustomerCode = Column(String(length=250))
	TemplateCode = Column(String(length=250))
	VlanDownLink = Column(String(length=250))
	FrameId = Column(String(length=250))
	SlotId = Column(String(length=250))
	PortId = Column(String(length=250))
	OntSn = Column(String(length=250))
	OntId = Column(String(length=250))
	OntLineProfileId = Column(String(length=250))
	OntSrvProfileId = Column(String(length=250))
	VlanId = Column(String(length=250))
	VlanFrameId = Column(String(length=250))
	VlanSlotId = Column(String(length=250))
	VlanPortId = Column(String(length=250))
	FdtNo = Column(String(length=250))
	FatNo = Column(String(length=250))
	NativeVlanEth1 = Column(String(length=250))
	NativeVlanEth2 = Column(String(length=250))
	NativeVlanEth3 = Column(String(length=250))
	NativeVlanEth4 = Column(String(length=250))
	VlanAttribut = Column(String(length=250))
	ClientStatus = Column(String(length=250))
	Remark = Column(LONGTEXT)
	Response = Column(LONGTEXT)
	ActiveStatus = Column(Integer)
	CreatedBy = Column(String(length=250), default="telnet")
	CreatedDate = Column(DateTime, default=func.now())
	UpdatedBy = Column(String(length=250), index=True, server_default="telnet")
	UpdatedDate = Column(DateTime, server_default=func.now(), onupdate=func.now())

	def __repr__(self):
		return '<Code: {}>'.format(self.Code)

	def count(self):
		return Clients.query.filter(Clients.HostCode==self.HostCode,Clients.ClientStatus=="Active").count()

	def list(self):
		return Clients.query.all()

	def show(self):
		return Clients.query.filter(Clients.Code==self.Code).all()

	def save(self):
		db_session.add(self)
		db_session.commit()

	def update(self):
		db_session.query(Clients).filter(Clients.Code == self.Code).update({Clients.OntId:self.OntId})
		db_session.commit()

	def update_sn(self):
		db_session.query(Clients).filter(Clients.Code == self.Code).update({Clients.OntSn:self.OntSn, Clients.Response:self.Response})
		db_session.commit()

	def update_response(self):
		db_session.query(Clients).filter(Clients.Code == self.Code).update({Clients.Response:self.Response})
		db_session.commit()

	def update_idx(self):
		db_session.query(Clients).filter(Clients.Code == self.Code).update({Clients.OntSn:self.OntSn, Clients.OntId:self.OntId, Clients.Response:self.Response})
		db_session.commit()

	def delete(self):
		Clients.query.filter_by(Clients.Code==self.Code).delete()

	def delete_all(self):
		Clients.query.delete()
