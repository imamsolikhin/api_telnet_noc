"""
Models for the Api app
"""

# local imports
from ...config.db import Base, db_session
from sqlalchemy import Column, Integer, String, DateTime, Text, Numeric, func

class DbaProfileLast(Base):
	"""
	This class represents the ont table.
	"""

	# Ensures table will be named in plural and not in singular as in
	# the name of the model
	__tablename__ = 'olt_dba_profile_last'

	Code = Column(String(length=250), primary_key=True)
	HostCode = Column(String(length=250))
	ProfileId = Column(String(length=250))
	Type = Column(String(length=250))
	Bandwidth = Column(String(length=250))
	Fix = Column(String(length=250))
	Assure = Column(String(length=250))
	Max = Column(String(length=250))
	Bind = Column(String(length=250))
	ActiveStatus = Column(Integer)
	CreatedBy = Column(String(length=250), default="telnet")
	CreatedDate = Column(DateTime, default=func.now())
	UpdatedBy = Column(String(length=250), index=True, server_default="telnet")
	UpdatedDate = Column(DateTime, server_default=func.now(), onupdate=func.now())

	def __repr__(self):
		return '<Code: {}>'.format(self.Code)

	def count(self):
		return DbaProfileLast.query.filter(DbaProfileLast.HostCode==self.HostCode).count()

	def list(self):
		return DbaProfileLast.query.all()

	def show(self):
		return DbaProfileLast.query.filter(DbaProfileLast.HostCode==self.HostCode).all()

	def save(self):
		db_session.add(self)
		db_session.commit()

	def delete(self):
		DbaProfileLast.query.filter_by(HostCode=self.HostCode).delete()

	def delete_all(self):
		DbaProfileLast.query.delete()
