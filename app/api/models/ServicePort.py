"""
Models for the Api app
"""

# local imports
from ...config.db import Base, db_session
from sqlalchemy import Column, Integer, String, DateTime, Text, Numeric, func

class ServicePort(Base):
	"""
	This class represents the ont table.
	"""

	# Ensures table will be named in plural and not in singular as in
	# the name of the model
	__tablename__ = 'olt_service_port'

	Code = Column(String(length=250), primary_key=True)
	HostCode = Column(String(length=250))
	Index = Column(String(length=250))
	VlanId = Column(String(length=250))
	VlanAttr = Column(String(length=250))
	Type = Column(String(length=250))
	FrameId = Column(String(length=250))
	SlotId = Column(String(length=250))
	PortId = Column(String(length=250))
	VPI = Column(String(length=250))
	VCI = Column(String(length=250))
	FlowType = Column(String(length=250))
	FlowPara = Column(String(length=250))
	Rx = Column(String(length=250))
	Tx = Column(String(length=250))
	State = Column(String(length=250))
	ActiveStatus = Column(Integer)
	CreatedBy = Column(String(length=250), default="telnet")
	CreatedDate = Column(DateTime, default=func.now())
	UpdatedBy = Column(String(length=250), index=True, server_default="telnet")
	UpdatedDate = Column(DateTime, server_default=func.now(), onupdate=func.now())

	def __repr__(self):
		return '<Code: {}>'.format(self.Code)

	def count_id(self):
		return ServicePort.query.filter(ServicePort.Code==self.Code).count()

	def count(self):
		return ServicePort.query.filter(ServicePort.HostCode==self.HostCode).count()

	def list(self):
		return ServicePort.query.all()

	def count(self):
		return ServicePort.query.filter(ServicePort.HostCode==self.HostCode).count()

	def count_by_vlan(self):
		return ServicePort.query.filter(ServicePort.HostCode==self.HostCode,ServicePort.VlanId==self.VlanId).count()

	def count_by_idx(self):
		return ServicePort.query.filter(ServicePort.HostCode==self.HostCode,ServicePort.Index==self.Index).count()

	def show(self):
		return ServicePort.query.filter(ServicePort.HostCode==self.HostCode).all()

	def save(self):
		db_session.add(self)
		db_session.commit()

	def delete_id(self):
		ServicePort.query.filter_by(Code=self.Code).delete()

	def delete(self):
		ServicePort.query.filter_by(HostCode=self.HostCode).delete()

	def delete_all(self):
		ServicePort.query.delete()
