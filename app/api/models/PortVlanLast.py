"""
Models for the Api app
"""

# local imports
from ...config.db import Base, db_session
from sqlalchemy import Column, Integer, String, DateTime, Text, Numeric, func

class PortVlanLast(Base):
	"""
	This class represents the ont table.
	"""

	# Ensures table will be named in plural and not in singular as in
	# the name of the model
	__tablename__ = 'olt_port_vlan_last'

	Code = Column(String(length=250), primary_key=True)
	HostCode = Column(String(length=250))
	Index = Column(String(length=250))
	Type = Column(String(length=250))
	Desc = Column(String(length=250))
	Attrib = Column(String(length=250))
	Frame = Column(String(length=250))
	Slot = Column(String(length=250))
	Port = Column(String(length=250))
	ActiveStatus = Column(Integer)
	CreatedBy = Column(String(length=250), default="telnet")
	CreatedDate = Column(DateTime, default=func.now())
	UpdatedBy = Column(String(length=250), index=True, server_default="telnet")
	UpdatedDate = Column(DateTime, server_default=func.now(), onupdate=func.now())

	def __repr__(self):
		return '<Code: {}>'.format(self.Code)

	def count(self):
		return PortVlanLast.query.filter(PortVlanLast.HostCode==self.HostCode).count()

	def list(self):
		return PortVlanLast.query.all()

	def show(self):
		return PortVlanLast.query.filter(PortVlanLast.HostCode==self.HostCode).all()

	def save(self):
		db_session.add(self)
		db_session.commit()

	def delete(self):
		PortVlanLast.query.filter_by(HostCode=self.HostCode).delete()

	def delete_all(self):
		PortVlanLast.query.delete()
