"""
Models for the Api app
"""

# local imports
from ...config.db import Base, db_session
from sqlalchemy import Column, Integer, String, DateTime, Text, Numeric, func

class HostTrafficTable(Base):
	"""
	This class represents the ont table.
	"""

	# Ensures table will be named in plural and not in singular as in
	# the name of the model
	__tablename__ = 'olt_traffic_table'

	Code = Column(String(length=250), primary_key=True)
	HostCode = Column(String(length=250))
	Index = Column(String(length=250))
	Name = Column(String(length=250))
	Cir = Column(String(length=250))
	Cbs = Column(String(length=250))
	Pir = Column(String(length=250))
	Pbs = Column(String(length=250))
	Priority = Column(String(length=250))
	ActiveStatus = Column(Integer)
	CreatedBy = Column(String(length=250), default="telnet")
	CreatedDate = Column(DateTime, default=func.now())
	UpdatedBy = Column(String(length=250), index=True, server_default="telnet")
	UpdatedDate = Column(DateTime, server_default=func.now(), onupdate=func.now())

	def __repr__(self):
		return '<Code: {}>'.format(self.Code)

	def count(self):
		return HostTrafficTable.query.filter(HostTrafficTable.HostCode==self.HostCode).count()

	def list(self):
		return HostTrafficTable.query.all()

	def count_idx(self):
		return HostTrafficTable.query.filter(HostTrafficTable.HostCode==self.HostCode,HostTrafficTable.Index==self.Index).count()

	def show(self):
		return HostTrafficTable.query.filter(HostTrafficTable.HostCode==self.HostCode).all()

	def save(self):
		db_session.add(self)
		db_session.commit()

	def delete(self):
		HostTrafficTable.query.filter_by(HostCode=self.HostCode).delete()

	def delete_by_code(self):
		HostTrafficTable.query.filter_by(Code=self.Code).delete()

	def delete_all(self):
		HostTrafficTable.query.delete()
