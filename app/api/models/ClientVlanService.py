"""
Models for the Api app
"""

# local imports
from ...config.db import Base, db_session
from sqlalchemy import Column, Integer, String, DateTime, Text, Numeric, func

class ClientVlanService(Base):
	"""
	This class represents the host table.
	"""

	# Ensures table will be named in plural and not in singular as in
	# the name of the model
	__tablename__ = 'mst_customer_noc_service'

	Code = Column(String(length=250), primary_key=True)
	CustomerCode = Column(String(length=250))
	ServicePortId = Column(String(length=250))
	Vlan = Column(String(length=250))
	GemPort = Column(String(length=250))
	UserVlan = Column(String(length=250))
	InnerVlan = Column(String(length=250))
	TagTransform = Column(String(length=250))
	TrafficTable = Column(String(length=250))
	Remark = Column(String(length=250))

	ActiveStatus = Column(Integer)
	CreatedBy = Column(String(length=250), default="telnet")
	CreatedDate = Column(DateTime, default=func.now())
	UpdatedBy = Column(String(length=250), index=True, server_default="telnet")
	UpdatedDate = Column(DateTime, server_default=func.now(), onupdate=func.now())

	def __repr__(self):
		return '<Code: {}>'.format(self.Code)

	def list(self):
		return ClientVlanService.query.all()

	def show(self):
		return ClientVlanService.query.filter(ClientVlanService.CustomerCode==self.CustomerCode).all()

	def save(self):
		db_session.add(self)
		db_session.commit()

	def update(self):
		db_session.query(ClientVlanService).filter(ClientVlanService.Code == self.Code).update({ClientVlanService.ServicePortId:self.ServicePortId,ClientVlanService.GemPort:self.GemPort})
		db_session.commit()

	def delete(self):
		ClientVlanService.query.filter_by(ClientVlanService.Code==self.Code).delete()

	def delete_all(self):
		ClientVlanService.query.delete()
