"""
Models for the Api app
"""

# local imports
from ...config.db import Base, db_session
from sqlalchemy import Column, Integer, String, DateTime, Text, Numeric, func

class OntInterface(Base):
	"""
	This class represents the ont table.
	"""

	# Ensures table will be named in plural and not in singular as in
	# the name of the model
	__tablename__ = 'olt_interface_gpon_ont'

	Code = Column(String(length=250), primary_key=True)
	HostCode = Column(String(length=250))
	FrameId = Column(String(length=250))
	SlotId = Column(String(length=250))
	PortId = Column(String(length=250))
	OntId = Column(String(length=250))
	OntSn = Column(String(length=250))
	Flag = Column(String(length=250))
	Run = Column(String(length=250))
	Config = Column(String(length=250))
	Match = Column(String(length=250))
	Protect = Column(String(length=250))
	ActiveStatus = Column(Integer)
	CreatedBy = Column(String(length=250), default="telnet")
	CreatedDate = Column(DateTime, default=func.now())
	UpdatedBy = Column(String(length=250), index=True, server_default="telnet")
	UpdatedDate = Column(DateTime, server_default=func.now(), onupdate=func.now())

	def __repr__(self):
		return '<Code: {}>'.format(self.Code)

	def count(self):
		return OntInterface.query.filter(OntInterface.HostCode==self.HostCode).count()

	def list(self):
		return OntInterface.query.all()

	def count(self):
		return OntInterface.query.filter(OntInterface.HostCode==self.HostCode).count()

	def count_sn(self):
		return OntInterface.query.filter(
					OntInterface.HostCode==self.HostCode,
					OntInterface.FrameId==self.FrameId,
					OntInterface.SlotId==self.SlotId,
					OntInterface.PortId==self.PortId,
					OntInterface.OntSn==self.OntSn
				).count()

	def count_flag(self):
		return OntInterface.query.filter(
					OntInterface.HostCode==self.HostCode,
					OntInterface.FrameId==self.FrameId,
					OntInterface.SlotId==self.SlotId,
					OntInterface.PortId==self.PortId,
					OntInterface.OntSn==self.OntSn,
					OntInterface.Flag==self.Flag
				).count()

	def count_idx(self):
		return OntInterface.query.filter(
					OntInterface.HostCode==self.HostCode,
					OntInterface.FrameId==self.FrameId,
					OntInterface.SlotId==self.SlotId,
					OntInterface.PortId==self.PortId,
					OntInterface.OntId==self.OntId
				).count()

	def dataMaxId(self):
		return OntInterface.query.filter(
			OntInterface.HostCode==self.HostCode,
			OntInterface.FrameId==self.FrameId,
			OntInterface.SlotId==self.SlotId,
			OntInterface.PortId==self.PortId
			).order_by(OntInterface.OntId.desc()).limit(1)

	def show(self):
		return OntInterface.query.filter(OntInterface.HostCode==self.HostCode).all()

	def show_bysn(self):
		return OntInterface.query.filter(OntInterface.OntSn==self.OntSn).all()

	def save(self):
		db_session.add(self)
		db_session.commit()

	def delete(self):
		OntInterface.query.filter_by(HostCode=self.HostCode).delete()

	def delete_all(self):
		OntInterface.query.delete()
