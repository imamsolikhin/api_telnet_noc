"""
Models for the Api app
"""

# local imports
from ...config.db import Base, db_session
from sqlalchemy import Column, Integer, String, DateTime, Text, Numeric, func

class Customer(Base):
	"""
	This class represents the host table.
	"""

	# Ensures table will be named in plural and not in singular as in
	# the name of the model
	__tablename__ = 'mst_customer_erp'

	Code = Column(String(length=250), primary_key=True)
	Name = Column(String(length=250))
	LinkID = Column(String(length=250))
	MemberID = Column(String(length=250))
	JobTitle = Column(String(length=250))
	ContactPerson = Column(String(length=250))
	Email = Column(String(length=250))
	Phone1 = Column(String(length=250))
	Phone2 = Column(String(length=250))
	PhoneNumber = Column(String(length=250))
	NPWP = Column(String(length=250))
	Fax = Column(String(length=250))
	Address = Column(String(length=250))
	CityCode = Column(String(length=250))
	City = Column(String(length=250))
	Province = Column(String(length=250))
	ZipCode = Column(String(length=250))
	Website = Column(String(length=250))
	IDCard = Column(String(length=250))
	IDCardNumber = Column(String(length=250))
	IDCardExpired = Column(String(length=250))
	RegistrationDate = Column(String(length=250))
	ProductCode = Column(String(length=250))
	ClientStatus = Column(String(length=250))
	ProductSite = Column(String(length=250))
	ProductGrup = Column(String(length=250))
	ProductPort = Column(String(length=250))
	ProductCategory = Column(String(length=250))
	ProductName = Column(String(length=250))
	ProductNotes = Column(String(length=250))
	BaseTransmissionStationCode = Column(String(length=250))

	ActiveStatus = Column(Integer)
	CreatedBy = Column(String(length=250), default="telnet")
	CreatedDate = Column(DateTime, default=func.now())
	UpdatedBy = Column(String(length=250), index=True, server_default="telnet")
	UpdatedDate = Column(DateTime, server_default=func.now(), onupdate=func.now())

	def __repr__(self):
		return '<Code: {}>'.format(self.Code)

	def list(self):
		return Customer.query.all()

	def show(self):
		return Customer.query.filter(Customer.Code==self.Code).all()

	def save(self):
		db_session.add(self)
		db_session.commit()

	def update(self):
		db_session.query(Customer).filter(Customer.Code == self.Code).update({Customer.CheckStatus:self.CheckStatus})
		db_session.commit()

	def delete(self):
		Customer.query.filter_by(Customer.Code==self.Code).delete()

	def delete_all(self):
		Customer.query.delete()
