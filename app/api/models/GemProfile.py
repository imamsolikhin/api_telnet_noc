"""
Models for the Api app
"""

# local imports
from ...config.db import Base, db_session
from sqlalchemy import Column, Integer, String, DateTime, Text, Numeric, func

class GemProfile(Base):
	"""
	This class represents the ont table.
	"""

	# Ensures table will be named in plural and not in singular as in
	# the name of the model
	__tablename__ = 'olt_gem_profile'

	Code = Column(String(length=250), primary_key=True)
	HostCode = Column(String(length=250))
	ProfileId = Column(String(length=250))
	GemId = Column(String(length=250))
	GemMapping = Column(String(length=250))
	GemVlan = Column(String(length=250))
	Priority = Column(String(length=250))
	Type = Column(String(length=250))
	Port = Column(String(length=250))
	Bundle = Column(String(length=250))
	Flow = Column(String(length=250))
	Transparent = Column(String(length=250))
	ActiveStatus = Column(Integer)
	CreatedBy = Column(String(length=250), default="telnet")
	CreatedDate = Column(DateTime, default=func.now())
	UpdatedBy = Column(String(length=250), index=True, server_default="telnet")
	UpdatedDate = Column(DateTime, server_default=func.now(), onupdate=func.now())

	def __repr__(self):
		return '<Code: {}>'.format(self.Code)

	def count_id(self):
		return GemProfile.query.filter(GemProfile.Code==self.Code).count()

	def count(self):
		return GemProfile.query.filter(GemProfile.HostCode==self.HostCode).count()

	def list(self):
		return GemProfile.query.all()

	def count(self):
		return GemProfile.query.filter(GemProfile.HostCode==self.HostCode,GemProfile.ProfileId==self.ProfileId,GemProfile.GemId==self.GemId).count()

	def count_by_vlan(self):
		return GemProfile.query.filter(GemProfile.HostCode==self.HostCode,GemProfile.ProfileId==self.ProfileId,GemProfile.GemVlan==self.GemVlan).count()

	def count_by_id(self):
		return GemProfile.query.filter(GemProfile.HostCode==self.HostCode,GemProfile.ProfileId==self.ProfileId,GemProfile.GemId==self.GemId).count()

	def show_by_vlan(self):
		return GemProfile.query.filter(GemProfile.HostCode==self.HostCode,GemProfile.ProfileId==self.ProfileId,GemProfile.GemVlan==self.GemVlan).limit(1).all()

	def show(self):
		return GemProfile.query.filter(GemProfile.HostCode==self.HostCode).all()

	def save(self):
		db_session.add(self)
		db_session.commit()

	def delete(self):
		GemProfile.query.filter_by(HostCode=self.HostCode).delete()

	def delete_by_code(self):
		GemProfile.query.filter_by(Code=self.Code).delete()

	def delete_all(self):
		GemProfile.query.delete()
