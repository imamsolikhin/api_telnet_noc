"""
Models for the Api app
"""

# local imports
from ...config.db import Base, db_session
from sqlalchemy import Column, Integer, String, DateTime, Text, Numeric, func

class Signal(Base):
	"""
	This class represents the host table.
	"""

	# Ensures table will be named in plural and not in singular as in
	# the name of the model
	__tablename__ = 'olt_signal_formula'

	Code = Column(String(length=250), primary_key=True)
	Formula = Column(String(length=250))
	TxPower = Column(Numeric)
	Distance = Column(Numeric)
	LossDb = Column(Numeric)
	LossFix = Column(Numeric)
	ActiveStatus = Column(Integer)
	CreatedBy = Column(String(length=250), default="telnet")
	CreatedDate = Column(DateTime, default=func.now())
	UpdatedBy = Column(String(length=250), index=True, server_default="telnet")
	UpdatedDate = Column(DateTime, server_default=func.now(), onupdate=func.now())

	def __repr__(self):
		return '<Code: {}>'.format(self.Code)

	def list(self):
		return Signal.query.all()

	def show(self):
		Signal.query.get(Code=self.Code)

	def save(self):
		db_session.add(self)
		db_session.commit()

	def delete(self):
		Signal.query.filter_by(Code=self.Code).delete()

	def delete_all(self):
		Signal.query.delete()
