"""
Models for the Api app
"""

# local imports
from ...config.db import Base, db_session
from sqlalchemy import Column, Integer, String, DateTime, Text, Numeric, func

class ClientsTools(Base):
	"""
	This class represents the ont table.
	"""

	# Ensures table will be named in plural and not in singular as in
	# the name of the model
	__tablename__ = 'mst_customer_noc_tools'

	Code = Column(String(length=250), primary_key=True)
	ClientsCode = Column(String(length=250))
	Version = Column(Text)
	OntInfo = Column(Text)
	WanInfo = Column(Text)
	Opticalinfo = Column(Text)
	Registerinfo = Column(Text)
	MacAddress = Column(Text)
	PortState = Column(Text)
	PortAttribute = Column(Text)
	FecCrcError = Column(Text)
	ActiveStatus = Column(Integer)
	CreatedBy = Column(String(length=250), default="telnet")
	CreatedDate = Column(DateTime, default=func.now())
	UpdatedBy = Column(String(length=250), index=True, server_default="telnet")
	UpdatedDate = Column(DateTime, server_default=func.now(), onupdate=func.now())

	def __repr__(self):
		return '<Code: {}>'.format(self.Code)

	def count(self):
		return ClientsTools.query.filter(ClientsTools.Code==self.Code,ClientsTools.ClientsCode==self.Code).count()

	def list(self):
		return ClientsTools.query.all()

	def show(self):
		return ClientsTools.query.filter(ClientsTools.Code==self.Code,ClientsTools.ClientsCode==self.Code).all()

	def save(self):
		db_session.add(self)
		db_session.commit()

	def delete(self):
		ClientsTools.query.filter_by(Code=self.Code).delete()

	def delete_all(self):
		ClientsTools.query.delete()

	def update_Version(self):
		db_session.query(ClientsTools).filter(ClientsTools.Code == self.Code).update({ClientsTools.Version:self.Version})
		db_session.commit()

	def update_OntInfo(self):
		db_session.query(ClientsTools).filter(ClientsTools.Code == self.Code).update({ClientsTools.OntInfo:self.OntInfo})
		db_session.commit()

	def update_WanInfo(self):
		db_session.query(ClientsTools).filter(ClientsTools.Code == self.Code).update({ClientsTools.WanInfo:self.WanInfo})
		db_session.commit()

	def update_Opticalinfo(self):
		db_session.query(ClientsTools).filter(ClientsTools.Code == self.Code).update({ClientsTools.Opticalinfo:self.Opticalinfo})
		db_session.commit()

	def update_Registerinfo(self):
		db_session.query(ClientsTools).filter(ClientsTools.Code == self.Code).update({ClientsTools.Registerinfo:self.Registerinfo})
		db_session.commit()

	def update_MacAddress(self):
		db_session.query(ClientsTools).filter(ClientsTools.Code == self.Code).update({ClientsTools.MacAddress:self.MacAddress})
		db_session.commit()

	def update_PortState(self):
		db_session.query(ClientsTools).filter(ClientsTools.Code == self.Code).update({ClientsTools.PortState:self.PortState})
		db_session.commit()

	def update_PortAttribute(self):
		db_session.query(ClientsTools).filter(ClientsTools.Code == self.Code).update({ClientsTools.PortAttribute:self.PortAttribute})
		db_session.commit()

	def update_FecCrcError(self):
		db_session.query(ClientsTools).filter(ClientsTools.Code == self.Code).update({ClientsTools.FecCrcError:self.FecCrcError})
		db_session.commit()
