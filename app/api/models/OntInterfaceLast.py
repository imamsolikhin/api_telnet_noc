"""
Models for the Api app
"""

# local imports
from ...config.db import Base, db_session
from sqlalchemy import Column, Integer, String, DateTime, Text, Numeric, func

class OntInterfaceLast(Base):
	"""
	This class represents the ont table.
	"""

	# Ensures table will be named in plural and not in singular as in
	# the name of the model
	__tablename__ = 'olt_interface_gpon_ont_last'

	Code = Column(String(length=250), primary_key=True)
	HostCode = Column(String(length=250))
	FrameId = Column(String(length=250))
	SlotId = Column(String(length=250))
	PortId = Column(String(length=250))
	OntId = Column(String(length=250))
	OntSn = Column(String(length=250))
	Flag = Column(String(length=250))
	Run = Column(String(length=250))
	Config = Column(String(length=250))
	Match = Column(String(length=250))
	Protect = Column(String(length=250))
	ActiveStatus = Column(Integer)
	CreatedBy = Column(String(length=250), default="telnet")
	CreatedDate = Column(DateTime, default=func.now())
	UpdatedBy = Column(String(length=250), index=True, server_default="telnet")
	UpdatedDate = Column(DateTime, server_default=func.now(), onupdate=func.now())

	def __repr__(self):
		return '<Code: {}>'.format(self.Code)

	def count(self):
		return OntInterfaceLast.query.filter(OntInterfaceLast.HostCode==self.HostCode).count()

	def list(self):
		return OntInterfaceLast.query.all()

	def count(self):
		return OntInterfaceLast.query.filter(OntInterfaceLast.HostCode==self.HostCode).count()

	def show(self):
		return OntInterfaceLast.query.filter(OntInterfaceLast.HostCode==self.HostCode).all()

	def save(self):
		db_session.add(self)
		db_session.commit()

	def delete(self):
		OntInterfaceLast.query.filter_by(HostCode=self.HostCode).delete()

	def delete_all(self):
		OntInterfaceLast.query.delete()
