"""
Models for the Api app
"""

# local imports
from ...config.db import Base, db_session
from sqlalchemy import Column, Integer, String, DateTime, Text, Numeric, func

class Host(Base):
	"""
	This class represents the host table.
	"""

	# Ensures table will be named in plural and not in singular as in
	# the name of the model
	__tablename__ = 'olt_host'

	Code = Column(String(length=250), primary_key=True)
	Hostname = Column(String(length=250))
	IpAddress = Column(String(length=250))
	Username = Column(String(length=250))
	Password = Column(String(length=250))
	Port = Column(String(length=250))
	FrameId = Column(String(length=250))
	SlotId = Column(String(length=250))
	PortId = Column(String(length=250))
	CheckStatus = Column(String(length=250))
	CurrentConfig = Column(Text)
	CheckDate = Column(DateTime, index=True, server_default=func.now(), onupdate=func.now())
	ActiveStatus = Column(Integer)
	CreatedBy = Column(String(length=250), default="telnet")
	CreatedDate = Column(DateTime, default=func.now())
	UpdatedBy = Column(String(length=250), index=True, server_default="telnet")
	UpdatedDate = Column(DateTime, server_default=func.now(), onupdate=func.now())

	def __repr__(self):
		return '<Code: {}>'.format(self.Code)

	def list(self):
		return Host.query.all()

	def list_active(self):
		return Host.query.filter(Host.ActiveStatus==1).all()

	def show(self):
		return Host.query.filter(Host.Code==self.Code).all()

	def save(self):
		db_session.add(self)
		db_session.commit()

	def update(self):
		db_session.query(Host).filter(Host.Code == self.Code).update({Host.CheckStatus:self.CheckStatus,Host.CurrentConfig:self.CurrentConfig})
		db_session.commit()
		db_session.remove()

	def update_config(self):
		db_session.query(Host).filter(Host.Code == self.Code).update({Host.CheckStatus:self.CheckStatus,Host.CheckDate:func.now(),Host.CurrentConfig:self.CurrentConfig})
		db_session.commit()
		db_session.remove()

	def delete(self):
		Host.query.filter_by(Host.Code==self.Code).delete()

	def delete_all(self):
		Host.query.delete()
