"""
Main application file
"""

# third-party imports
from flask import Flask, render_template

# local imports
from .config.routes import api_app

def create_app(config):
	app = Flask(__name__)
	app.add_url_rule("/", "home", home)
	app.add_url_rule("/api/", "api", api)

	# load configurations
	app.config.from_object(config)

	# initialize api app
	app.register_blueprint(api_app, url_prefix='/api')

	return app

def home():
	return render_template('index.html')

def api():
	return render_template('api.html')
