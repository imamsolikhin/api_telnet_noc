"""
Command for running the application
"""
# from flask import Flask

# local imports
from config import DevelopmentConfig, ProductionConfig, StagingConfig

from app.config.db import init_db, db_session
from app import create_app
from rq import Worker, Queue, Connection
import redis
import os

redis_url = 'redis://172.16.32.150:6379'
conn = redis.from_url(redis_url)

# main entry point of the app
# app = create_app(ProductionConfig)
app = create_app(DevelopmentConfig)
init_db()

if __name__ == '__main__':
	with Connection(conn):
		worker = Worker(list(map(Queue, listen)))
		worker.work()
	init_db()
	app.run(threaded=True)

@app.teardown_appcontext
def shutdown_session(exception=None):
	db_session.remove()
