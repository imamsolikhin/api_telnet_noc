## Summary:
The REST APIs are built using Python Flask framework. Docker containers are used for development environment.

It contains the below mentioned endpoints:
- a POST '/grab_and_save'
- a GET '/last'

There are many ways to setup your project folder structure. One is by its function and another is app based structure which means things are grouped bp application. I have used app based approach for this task.

## Setup
pip install -r reqs.txt


## Project Structure (App Based):
```bash
FlaskProject/
├── Dockerfile
├── Makefile
├── README.md
├── app/
│   ├── __init__.py
│   ├── api/
│   │   ├── __init__.py
│   │   ├── models.py
│   │   ├── resources.py
│   │   ├── routes.py
│   │   └── schemas.py
│   ├── constants.py
│   ├── db.py
│   ├── settings.py
│   └── utils.py
├── config.py
├── docker-compose.yml
├── entrypoint.sh
├── migrate.py
├── migrations/
│   ├── README
│   ├── alembic.ini
│   ├── env.py
│   ├── script.py.mako
│   └── versions
├── .gitignore
├── requirements.txt
└── run.py
```
