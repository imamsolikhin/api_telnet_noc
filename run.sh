app="api_telnet_noc"
docker rm $(docker stop $(docker ps -a -q --filter ancestor=${app} --format="{{.ID}}"))
docker build -t ${app} .
docker run -i -t -p 8006:8006 ${app}
