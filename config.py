"""
Configuration file for the application.
"""

#local import
import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
	"""
	Common configurations
	"""
	# Put any configurations here that are common across all environments
	SECRET_KEY = os.environ['SECRET_KEY']
	DB_USERNAME = os.environ['MYSQL_USER']
	DB_PASSWORD = os.environ['MYSQL_PASSWORD']
	DB_HOST = os.environ['MYSQL_HOST']
	DB_NAME = os.environ['MYSQL_DATABASE']
	DB_PORT = os.environ['MYSQL_PORT']
	DB_URI = "mysql+pymysql://%s:%s@%s:%s/%s"  % (DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME)
	SQLALCHEMY_DATABASE_URI = DB_URI
	SQLALCHEMY_POOL_SIZE = 100
	SQLALCHEMY_POOL_RECYCLE = 180
	SQLALCHEMY_POOL_TIMEOUT = 180

class ProductionConfig(Config):
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	SQLALCHEMY_ECHO = False
	DEBUG = False


class StagingConfig(Config):
	SQLALCHEMY_TRACK_MODIFICATIONS = True
	DEBUG = True


class DevelopmentConfig(Config):
	DEBUG = True
	SQLALCHEMY_TRACK_MODIFICATIONS = True
	SQLALCHEMY_ECHO = True
